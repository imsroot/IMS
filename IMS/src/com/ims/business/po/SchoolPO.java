package com.ims.business.po;


import com.ims.common.core.matatype.BasePO;
import java.math.BigDecimal;
import java.util.Date;
/**
 * 
 * 类描述： <b>学校信息表[ih_school]数据持久化对象</b>
 * 创建人：陈骑元
 * 邮箱：240823329@qq.com
 * 创建时间：2017-03-07 20:20:38
 * 修改人：
 * 修改时间：
 * 修改备注： 
 * @version 1.0
 */

public class SchoolPO extends BasePO {

	private static final long serialVersionUID = 1L;

	/**
	 * 学校编号
	 */
	private String school_id;
	
	/**
	 * 所属街镇
	 */
	private String street;
	
	/**
	 * 学校名称
	 */
	private String school_name;
	
	/**
	 * 名额
	 */
	private BigDecimal places;
	
	/**
	 * 学校地址
	 */
	private String school_address;
	
	/**
	 * 备注
	 */
	private String remark;
	
	/**
	 * 创建时间
	 */
	private Date create_time;
	
	/**
	 * 创建用户编号
	 */
	private String create_user_id;
	
	/**
	 * 修改时间
	 */
	private Date modify_time;
	
	/**
	 * 修改用户id
	 */
	private String modify_user_id;
	
	/**
	 * 学校类型
	 */
	private String school_type;
	
	// 新增辅助字段：已申请数量
	private String applyed;

	/**
	 * 学校编号
	 * 
	 * @return school_id
	 */
	public String getSchool_id() {
		return school_id;
	}
	
	/**
	 * 所属街镇
	 * 
	 * @return street
	 */
	public String getStreet() {
		return street;
	}
	
	/**
	 * 学校名称
	 * 
	 * @return school_name
	 */
	public String getSchool_name() {
		return school_name;
	}
	
	/**
	 * 名额
	 * 
	 * @return places
	 */
	public BigDecimal getPlaces() {
		return places;
	}
	
	/**
	 * 学校地址
	 * 
	 * @return school_address
	 */
	public String getSchool_address() {
		return school_address;
	}
	
	/**
	 * 备注
	 * 
	 * @return remark
	 */
	public String getRemark() {
		return remark;
	}
	
	/**
	 * 创建时间
	 * 
	 * @return create_time
	 */
	public Date getCreate_time() {
		return create_time;
	}
	
	/**
	 * 创建用户编号
	 * 
	 * @return create_user_id
	 */
	public String getCreate_user_id() {
		return create_user_id;
	}
	
	/**
	 * 修改时间
	 * 
	 * @return modify_time
	 */
	public Date getModify_time() {
		return modify_time;
	}
	
	/**
	 * 修改用户id
	 * 
	 * @return modify_user_id
	 */
	public String getModify_user_id() {
		return modify_user_id;
	}
	
	/**
	 * 学校类型
	 * 
	 * @return school_type
	 */
	public String getSchool_type() {
		return school_type;
	}
	

	/**
	 * 学校编号
	 * 
	 * @param school_id
	 */
	public void setSchool_id(String school_id) {
		this.school_id = school_id;
	}
	
	/**
	 * 所属街镇
	 * 
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
	/**
	 * 学校名称
	 * 
	 * @param school_name
	 */
	public void setSchool_name(String school_name) {
		this.school_name = school_name;
	}
	
	/**
	 * 名额
	 * 
	 * @param places
	 */
	public void setPlaces(BigDecimal places) {
		this.places = places;
	}
	
	/**
	 * 学校地址
	 * 
	 * @param school_address
	 */
	public void setSchool_address(String school_address) {
		this.school_address = school_address;
	}
	
	/**
	 * 备注
	 * 
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	/**
	 * 创建时间
	 * 
	 * @param create_time
	 */
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	
	/**
	 * 创建用户编号
	 * 
	 * @param create_user_id
	 */
	public void setCreate_user_id(String create_user_id) {
		this.create_user_id = create_user_id;
	}
	
	/**
	 * 修改时间
	 * 
	 * @param modify_time
	 */
	public void setModify_time(Date modify_time) {
		this.modify_time = modify_time;
	}
	
	/**
	 * 修改用户id
	 * 
	 * @param modify_user_id
	 */
	public void setModify_user_id(String modify_user_id) {
		this.modify_user_id = modify_user_id;
	}
	
	/**
	 * 学校类型
	 * 
	 * @param school_type
	 */
	public void setSchool_type(String school_type) {
		this.school_type = school_type;
	}

	public String getApplyed() {
		return applyed;
	}

	public void setApplyed(String applyed) {
		this.applyed = applyed;
	}
}