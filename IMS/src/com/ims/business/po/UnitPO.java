package com.ims.business.po;


import com.ims.common.core.matatype.BasePO;
import java.math.BigDecimal;
import java.util.Date;
/**
 * 
 * 类描述： <b>职能单位信息表[ih_unit]数据持久化对象</b>
 * 创建人：陈骑元
 * 邮箱：240823329@qq.com
 * 创建时间：2017-03-07 21:04:23
 * 修改人：
 * 修改时间：
 * 修改备注： 
 * @version 1.0
 */

public class UnitPO extends BasePO {

	private static final long serialVersionUID = 1L;

	/**
	 * 单位编号主键
	 */
	private String unit_id;
	
	/**
	 * 单位名称
	 */
	private String unit_name;
	
	/**
	 * 服务范围
	 */
	private String unit_serve;
	
	/**
	 * 单位类型
	 */
	private String unit_type;
	
	/**
	 * 单位地址
	 */
	private String unit_address;
	
	/**
	 * 单位电话
	 */
	private String unit_phone;
	
	/**
	 * 创建时间
	 */
	private Date create_time;
	
	/**
	 * 创建用户
	 */
	private String create_user_id;
	
	/**
	 * 修改用户
	 */
	private Date modify_time;
	
	/**
	 * 修改用户编号
	 */
	private String modify_user_id;
	
	/**
	 * 排序
	 */
	private BigDecimal sort_no;
	
	/**
	 * 备注
	 */
	private String remark;
	

	/**
	 * 单位编号主键
	 * 
	 * @return unit_id
	 */
	public String getUnit_id() {
		return unit_id;
	}
	
	/**
	 * 单位名称
	 * 
	 * @return unit_name
	 */
	public String getUnit_name() {
		return unit_name;
	}
	
	/**
	 * 服务范围
	 * 
	 * @return unit_serve
	 */
	public String getUnit_serve() {
		return unit_serve;
	}
	
	/**
	 * 单位类型
	 * 
	 * @return unit_type
	 */
	public String getUnit_type() {
		return unit_type;
	}
	
	/**
	 * 单位地址
	 * 
	 * @return unit_address
	 */
	public String getUnit_address() {
		return unit_address;
	}
	
	/**
	 * 单位电话
	 * 
	 * @return unit_phone
	 */
	public String getUnit_phone() {
		return unit_phone;
	}
	
	/**
	 * 创建时间
	 * 
	 * @return create_time
	 */
	public Date getCreate_time() {
		return create_time;
	}
	
	/**
	 * 创建用户
	 * 
	 * @return create_user_id
	 */
	public String getCreate_user_id() {
		return create_user_id;
	}
	
	/**
	 * 修改用户
	 * 
	 * @return modify_time
	 */
	public Date getModify_time() {
		return modify_time;
	}
	
	/**
	 * 修改用户编号
	 * 
	 * @return modify_user_id
	 */
	public String getModify_user_id() {
		return modify_user_id;
	}
	
	/**
	 * 排序
	 * 
	 * @return sort_no
	 */
	public BigDecimal getSort_no() {
		return sort_no;
	}
	
	/**
	 * 备注
	 * 
	 * @return remark
	 */
	public String getRemark() {
		return remark;
	}
	

	/**
	 * 单位编号主键
	 * 
	 * @param unit_id
	 */
	public void setUnit_id(String unit_id) {
		this.unit_id = unit_id;
	}
	
	/**
	 * 单位名称
	 * 
	 * @param unit_name
	 */
	public void setUnit_name(String unit_name) {
		this.unit_name = unit_name;
	}
	
	/**
	 * 服务范围
	 * 
	 * @param unit_serve
	 */
	public void setUnit_serve(String unit_serve) {
		this.unit_serve = unit_serve;
	}
	
	/**
	 * 单位类型
	 * 
	 * @param unit_type
	 */
	public void setUnit_type(String unit_type) {
		this.unit_type = unit_type;
	}
	
	/**
	 * 单位地址
	 * 
	 * @param unit_address
	 */
	public void setUnit_address(String unit_address) {
		this.unit_address = unit_address;
	}
	
	/**
	 * 单位电话
	 * 
	 * @param unit_phone
	 */
	public void setUnit_phone(String unit_phone) {
		this.unit_phone = unit_phone;
	}
	
	/**
	 * 创建时间
	 * 
	 * @param create_time
	 */
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	
	/**
	 * 创建用户
	 * 
	 * @param create_user_id
	 */
	public void setCreate_user_id(String create_user_id) {
		this.create_user_id = create_user_id;
	}
	
	/**
	 * 修改用户
	 * 
	 * @param modify_time
	 */
	public void setModify_time(Date modify_time) {
		this.modify_time = modify_time;
	}
	
	/**
	 * 修改用户编号
	 * 
	 * @param modify_user_id
	 */
	public void setModify_user_id(String modify_user_id) {
		this.modify_user_id = modify_user_id;
	}
	
	/**
	 * 排序
	 * 
	 * @param sort_no
	 */
	public void setSort_no(BigDecimal sort_no) {
		this.sort_no = sort_no;
	}
	
	/**
	 * 备注
	 * 
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	

}