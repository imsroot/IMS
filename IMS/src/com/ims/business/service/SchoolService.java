package com.ims.business.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ims.common.core.asset.IMSUtils;
import com.ims.common.core.matatype.Dto;
import com.ims.common.core.id.IMSId;
import com.ims.business.mapper.SchoolMapper;
import com.ims.business.po.SchoolPO;
import com.ims.common.core.asset.IMSCons;
import com.ims.common.core.matatype.Dtos;

/**
 * 
 * 类描述：<b>学校信息表[ih_school业务逻辑</b>
 * 创建人：陈骑元
 * 邮箱：240823329@qq.com
 * 创建时间：2017-03-07 20:20:38
 * 修改人：
 * 修改时间：
 * 修改备注： 
 * @version 1.0
 */
@Service
public class SchoolService {
  
    @Autowired
	private  SchoolMapper schoolMapper;
	
	/**
	 * 插入一个数据持久化对象(插入字段为传入PO实体的非空属性)
	 * <p> 防止DB字段缺省值需要程序中再次赋值
	 *
	 * @param schoolPO
	 *            要插入的数据持久化对象
	 * @return 返回影响行数
	 */
	@Transactional
	public int insert(SchoolPO schoolPO){
	
	     return schoolMapper.insert(schoolPO);
	     
	};
	/**
	 * 保存一个持久化对象 并返回一个Dto对象
	 * <p> 防止DB字段缺省值需要程序中再次赋值
	 *
	 * @param inDto Dto参数对象
	 *            
	 * @return 返回Dto对象
	 */
	@Transactional
	public Dto save(Dto inDto){
	   Dto outDto = Dtos.newDto();
	   SchoolPO schoolPO =new  SchoolPO();
	   schoolPO.setSchool_id(IMSId.uuid());
	   IMSUtils.copyProperties(inDto,  schoolPO); 
	   schoolPO.setCreate_time(IMSUtils.getDateTime());
	   schoolPO.setCreate_user_id(inDto.getString(IMSCons.LOGIN_USER_ID));
	   schoolPO.setModify_time(IMSUtils.getDateTime());
	   schoolPO.setModify_user_id(inDto.getString(IMSCons.LOGIN_USER_ID));
	   int row=schoolMapper.insert(schoolPO);
	   if(row>0){
		   outDto.setAppCode(IMSCons.SUCCESS);
		   outDto.setAppMsg("操作完成，学校信息新增成功。");
	   }else{
		   outDto.setAppCode(IMSCons.ERROR);
		   outDto.setAppMsg("操作失败，学校信息新增失败。");
	   }
	   return outDto;
	     
	};
	
	/**
	 * 插入一个数据持久化对象(含所有字段)
	 * 
	 * @param schoolPO
	 *            要插入的数据持久化对象
	 * @return 返回影响行数
	 */
    @Transactional
	public int insertAll(SchoolPO schoolPO){
	
	     return schoolMapper.insertAll(schoolPO);
	     
	};

	/**
	 * 根据主键修改数据持久化对象
	 * 
	 * @param schoolPO
	 *            要修改的数据持久化对象
	 * @return int 返回影响行数
	 */
	 @Transactional
	public int updateByKey(SchoolPO schoolPO){
	
	     return schoolMapper.updateByKey(schoolPO);
	
	};
	/**
	 * 根据主键修改数据持久化对象
	 * 
	 * @param inDto传入参数
	 *            要修改的数据持久化对象
	 * @return Dto 返回影Dto对象
	 */
    @Transactional
	public Dto update(Dto inDto){
	   Dto outDto = Dtos.newDto();
	   SchoolPO schoolPO =new  SchoolPO();
	   IMSUtils.copyProperties(inDto,  schoolPO); 
	   schoolPO.setModify_time(IMSUtils.getDateTime());
	   schoolPO.setModify_user_id(inDto.getString(IMSCons.LOGIN_USER_ID));
	   int row=schoolMapper.updateByKey(schoolPO);
	   if(row>0){
		   outDto.setAppCode(IMSCons.SUCCESS);
		   outDto.setAppMsg("操作完成，学校信息修改成功。");
	   }else{
		   outDto.setAppCode(IMSCons.ERROR);
		   outDto.setAppMsg("操作失败，学校信息修改失败。");
	   }
	   return outDto;
	     
	};
	/**
	 * 根据主键查询并返回数据持久化对象
	 * 
	 * @return SchoolPO
	 */
	public SchoolPO selectByKey( String school_id){
	
	    return schoolMapper.selectByKey(school_id);
	   
	};

	/**
	 * 根据唯一组合条件查询并返回数据持久化对象
	 * 
	 * @return SchoolPO
	 */
	public SchoolPO selectOne(Dto pDto){
	
	    return schoolMapper.selectOne(pDto);
	
	};

	/**
	 * 根据Dto查询并返回数据持久化对象集合
	 * 
	 * @return List<SchoolPO>
	 */
	public List<SchoolPO> list(Dto pDto){
	
	     return schoolMapper.list(pDto);
	
	};

	/**
	 * 根据Dto查询并返回分页数据持久化对象集合
	 * 
	 * @return List<SchoolPO>
	 */
	public List<SchoolPO> listPage(Dto pDto){
	
	     return schoolMapper.listPage( pDto);
	
	};
		
	/**
	 * 根据Dto模糊查询并返回数据持久化对象集合(字符型字段模糊匹配，其余字段精确匹配)
	 * 
	 * @return List<SchoolPO>
	 */
	public List<SchoolPO> like(Dto pDto){
	  
	      return schoolMapper.like( pDto);
	
	};

	/**
	 * 根据Dto模糊查询并返回分页数据持久化对象集合(字符型字段模糊匹配，其余字段精确匹配)
	 * 
	 * @return List<SchoolPO>
	 */
	public List<SchoolPO> likePage(Dto pDto){
	
	    return schoolMapper.likePage( pDto);
	
	};
	
	/**
	 * 根据主键删除数据持久化对象
	 *
	 * @return 影响行数
	 */
   @Transactional
    public int  deleteByKey( String school_id){
	
	    return schoolMapper.deleteByKey(school_id);
	};
	
	/**
	 * 根据主键删除数据持久化对象
	 *
	 * @return 返回一个Dto对象
	 */
	@Transactional
	public Dto delete( String school_id){
	   Dto outDto = Dtos.newDto();
	   int row=schoolMapper.deleteByKey(school_id);
	   if(row>0){
		   outDto.setAppCode(IMSCons.SUCCESS);
		   outDto.setAppMsg("操作完成，学校信息表数据删除成功。");
	   }else{
		   outDto.setAppCode(IMSCons.ERROR);
		   outDto.setAppMsg("操作失败，学校信息表数据删除失败。");
	   }
	    return outDto;
	};
	
	/**
	 * 批量根据主键删除数据持久化对象
	 *
	 * @return 影响行数
	 */
	@Transactional
	public int batchDeleteByKey(List<String> school_idList){
	     
	     return schoolMapper.batchDeleteByKey(school_idList);
	
	};
	/**
	 * 批量根据主键删除数据持久化对象
	 *
	 * @return 返回一个Dto对象
	 */
	@Transactional
	public Dto batchDelete(List<String> school_idList){
	    Dto outDto = Dtos.newDto();
	    int row=schoolMapper.batchDeleteByKey(school_idList);
	    if(row>0){
	       outDto.setAppCode(IMSCons.SUCCESS);
		   outDto.setAppMsg("操作完成，删除学校信息成功。");
	    }else{
	       outDto.setAppCode(IMSCons.ERROR);
		   outDto.setAppMsg("操作失败，删除学校信息失败。");
	    }
	    return outDto;
	
	};
	
	/**
	 * 根据Dto统计行数
	 * 
	 * @param pDto
	 * @return
	 */
	public int rows(Dto pDto){
	
	    return schoolMapper.rows( pDto);
	
	};
	
	/**
	 * 根据数学表达式进行数学运算
	 * 
	 * @param pDto
	 * @return String
	 */
	public String calc(Dto pDto){
	 
	    return schoolMapper.calc(pDto);
	
	};
	
}
