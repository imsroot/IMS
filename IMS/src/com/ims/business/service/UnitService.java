package com.ims.business.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ims.common.core.asset.IMSUtils;
import com.ims.common.core.matatype.Dto;
import com.ims.common.core.id.IMSId;
import com.ims.business.mapper.UnitMapper;
import com.ims.business.po.UnitPO;
import com.ims.common.core.asset.IMSCons;
import com.ims.common.core.matatype.Dtos;


/**
 * 
 * 类描述：<b>职能单位信息表[ih_unit业务逻辑</b>
 * 创建人：陈骑元
 * 邮箱：240823329@qq.com
 * 创建时间：2017-03-07 19:34:50
 * 修改人：
 * 修改时间：
 * 修改备注： 
 * @version 1.0
 */
@Service
public class UnitService {
  
    @Autowired
	private  UnitMapper unitMapper;
	
	/**
	 * 插入一个数据持久化对象(插入字段为传入PO实体的非空属性)
	 * <p> 防止DB字段缺省值需要程序中再次赋值
	 *
	 * @param unitPO
	 *            要插入的数据持久化对象
	 * @return 返回影响行数
	 */
	@Transactional
	public int insert(UnitPO unitPO){
	
	     return unitMapper.insert(unitPO);
	     
	};
	/**
	 * 保存一个持久化对象 并返回一个Dto对象
	 * <p> 防止DB字段缺省值需要程序中再次赋值
	 *
	 * @param inDto Dto参数对象
	 *            
	 * @return 返回Dto对象
	 */
	@Transactional
	public Dto save(Dto inDto){
	   Dto outDto = Dtos.newDto();
	   UnitPO unitPO =new  UnitPO();
	   unitPO.setUnit_id(IMSId.uuid());
	   IMSUtils.copyProperties(inDto,  unitPO); 
	   unitPO.setCreate_time(IMSUtils.getDateTime());
	   unitPO.setCreate_user_id(inDto.getString(IMSCons.LOGIN_USER_ID));
	   unitPO.setModify_time(IMSUtils.getDateTime());
	   unitPO.setModify_user_id(inDto.getString(IMSCons.LOGIN_USER_ID));
	   int row=unitMapper.insert(unitPO);
	   if(row>0){
		   outDto.setAppCode(IMSCons.SUCCESS);
		   outDto.setAppMsg("操作完成，单位信息新增成功。");
	   }else{
		   outDto.setAppCode(IMSCons.ERROR);
		   outDto.setAppMsg("操作失败，单位信息新增失败。");
	   }
	   return outDto;
	     
	};
	
	/**
	 * 插入一个数据持久化对象(含所有字段)
	 * 
	 * @param unitPO
	 *            要插入的数据持久化对象
	 * @return 返回影响行数
	 */
    @Transactional
	public int insertAll(UnitPO unitPO){
	
	     return unitMapper.insertAll(unitPO);
	     
	};

	/**
	 * 根据主键修改数据持久化对象
	 * 
	 * @param unitPO
	 *            要修改的数据持久化对象
	 * @return int 返回影响行数
	 */
	 @Transactional
	public int updateByKey(UnitPO unitPO){
	
	     return unitMapper.updateByKey(unitPO);
	
	};
	/**
	 * 根据主键修改数据持久化对象
	 * 
	 * @param inDto传入参数
	 *            要修改的数据持久化对象
	 * @return Dto 返回影Dto对象
	 */
    @Transactional
	public Dto update(Dto inDto){
	   Dto outDto = Dtos.newDto();
	   UnitPO unitPO =new  UnitPO();
	   IMSUtils.copyProperties(inDto,  unitPO); 
	   int row=unitMapper.updateByKey(unitPO);
	   if(row>0){
		   outDto.setAppCode(IMSCons.SUCCESS);
		   outDto.setAppMsg("操作完成，单位信息修改成功。");
	   }else{
		   outDto.setAppCode(IMSCons.ERROR);
		   outDto.setAppMsg("操作失败，单位信息修改失败。");
	   }
	   return outDto;
	     
	};
	/**
	 * 根据主键查询并返回数据持久化对象
	 * 
	 * @return UnitPO
	 */
	public UnitPO selectByKey( String unit_id){
	
	    return unitMapper.selectByKey(unit_id);
	   
	};

	/**
	 * 根据唯一组合条件查询并返回数据持久化对象
	 * 
	 * @return UnitPO
	 */
	public UnitPO selectOne(Dto pDto){
	
	    return unitMapper.selectOne(pDto);
	
	};

	/**
	 * 根据Dto查询并返回数据持久化对象集合
	 * 
	 * @return List<UnitPO>
	 */
	public List<UnitPO> list(Dto pDto){
	
	     return unitMapper.list(pDto);
	
	};

	/**
	 * 根据Dto查询并返回分页数据持久化对象集合
	 * 
	 * @return List<UnitPO>
	 */
	public List<UnitPO> listPage(Dto pDto){
	
	     return unitMapper.listPage( pDto);
	
	};
		
	/**
	 * 根据Dto模糊查询并返回数据持久化对象集合(字符型字段模糊匹配，其余字段精确匹配)
	 * 
	 * @return List<UnitPO>
	 */
	public List<UnitPO> like(Dto pDto){
	  
	      return unitMapper.like( pDto);
	
	};

	/**
	 * 根据Dto模糊查询并返回分页数据持久化对象集合(字符型字段模糊匹配，其余字段精确匹配)
	 * 
	 * @return List<UnitPO>
	 */
	public List<UnitPO> likePage(Dto pDto){
	
	    return unitMapper.likePage( pDto);
	
	};
	
	/**
	 * 根据主键删除数据持久化对象
	 *
	 * @return 影响行数
	 */
   @Transactional
    public int  deleteByKey( String unit_id){
	
	    return unitMapper.deleteByKey(unit_id);
	};
	
	/**
	 * 根据主键删除数据持久化对象
	 *
	 * @return 返回一个Dto对象
	 */
	@Transactional
	public Dto delete( String unit_id){
	   Dto outDto = Dtos.newDto();
	   int row=unitMapper.deleteByKey(unit_id);
	   if(row>0){
		   outDto.setAppCode(IMSCons.SUCCESS);
		   outDto.setAppMsg("操作完成，职能单位信息表数据删除成功。");
	   }else{
		   outDto.setAppCode(IMSCons.ERROR);
		   outDto.setAppMsg("操作失败，职能单位信息表数据删除失败。");
	   }
	    return outDto;
	};
	
	/**
	 * 批量根据主键删除数据持久化对象
	 *
	 * @return 影响行数
	 */
	@Transactional
	public int batchDeleteByKey(List<String> unit_idList){
	     
	     return unitMapper.batchDeleteByKey(unit_idList);
	
	};
	/**
	 * 批量根据主键删除数据持久化对象
	 *
	 * @return 返回一个Dto对象
	 */
	@Transactional
	public Dto batchDelete(List<String> unit_idList){
	    Dto outDto = Dtos.newDto();
	    int row=unitMapper.batchDeleteByKey(unit_idList);
	    if(row>0){
	       outDto.setAppCode(IMSCons.SUCCESS);
		   outDto.setAppMsg("操作完成，单位信息除成功。");
	    }else{
	       outDto.setAppCode(IMSCons.ERROR);
		   outDto.setAppMsg("操作失败，单位信息删除失败。");
	    }
	    return outDto;
	
	};
	
	/**
	 * 根据Dto统计行数
	 * 
	 * @param pDto
	 * @return
	 */
	public int rows(Dto pDto){
	
	    return unitMapper.rows( pDto);
	
	};
	
	/**
	 * 根据数学表达式进行数学运算
	 * 
	 * @param pDto
	 * @return String
	 */
	public String calc(Dto pDto){
	 
	    return unitMapper.calc(pDto);
	
	};
	
}
