package com.ims.business.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.ims.common.core.asset.IMSJson;
import com.ims.business.po.SchoolPO;
import com.ims.business.service.SchoolService;
import com.ims.common.core.asset.IMSCxt;
import com.ims.common.core.matatype.Dto;
import com.ims.common.core.matatype.Dtos;
import com.ims.common.core.asset.IMSFormater;

/**
 * 
 * 类描述：<b>学校信息表[ih_school]控制类</b>
 * 创建人：陈骑元
 * 邮箱：240823329@qq.com
 * 创建时间：2017-03-07 20:20:38
 * 修改人：
 * 修改时间：
 * 修改备注： 
 * @version 1.0
 */
@Controller
@RequestMapping(value = "business/school")
public class SchoolController {
  
    @Autowired
	private  SchoolService schoolService;
	
	
	/**
	 * 页面初始化
	 *
	 * @return
	 */
	@RequestMapping(value = "init")
	public ModelAndView init() {
		return new ModelAndView("business/school/schoolList.jsp");
	}
	
	/**
	 * 
	 * 分页查询学校信息表信息
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "listSchool")
	public void listSchool(HttpServletRequest request, HttpServletResponse response) {
		Dto pDto = Dtos.newDto(request);
		pDto.setOrder(" street DESC ");
		List<SchoolPO> schoolList =schoolService.likePage(pDto);
		String outString = IMSJson.toGridJson(schoolList, pDto.getPageTotal());
		IMSCxt.write(response, outString);
	}
	/**
	 * 
	 * 初始化新增学校信息表页面
	 * @return
	 * 
	 */
	@RequestMapping(value = "goAdd")
	public ModelAndView goAdd() {
		return new ModelAndView("business/school/addSchool.jsp");
	}
	/**
	 * 保存学校信息表信息
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveSchool")
	public void saveSchool(HttpServletRequest request, HttpServletResponse response) {
	    Dto inDto = Dtos.newDto(request);
		Dto outDto = schoolService.save(inDto);
		IMSCxt.write(response, IMSJson.toJson(outDto));
	}
	/**
	 * 查看学校信息表详情页面
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "goShow")
	public ModelAndView goShow( String school_id) {
	   ModelAndView modelAndView= new ModelAndView();
	   SchoolPO schoolPO=schoolService.selectByKey(school_id);
	   modelAndView.addObject("schoolPO",schoolPO);
	   modelAndView.setViewName("showSchool.jsp");
	   return modelAndView;
	}
	/**
	 * 初始化修改学校信息表页面
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "goModify")
	public ModelAndView goModify( String school_id) {
	   ModelAndView modelAndView= new ModelAndView();
	   SchoolPO schoolPO=schoolService.selectByKey(school_id);
	   modelAndView.addObject("schoolPO",schoolPO);
	   modelAndView.setViewName("business/school/modifySchool.jsp");
	   return modelAndView;
	}
	/**
	 * 修改学校信息表信息
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "updateSchool")
	public void updateSchool(HttpServletRequest request, HttpServletResponse response) {
		Dto inDto = Dtos.newDto(request);
		Dto outDto =schoolService.update(inDto);
		IMSCxt.write(response, IMSJson.toJson(outDto));
	}
	/**
	 * 删除学校信息表信息
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteSchool")
	public void deleteSchool( String school_id,HttpServletResponse response) {
		Dto outDto =schoolService.delete(school_id);
		IMSCxt.write(response, IMSJson.toJson(outDto));
	}
	/**
	 * 批量删除学校信息表信息
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "batchDeleteSchool")
	public void batchDeleteSchool( String school_ids,HttpServletResponse response) {
	    List<String> school_idList=IMSFormater.separatStringToList(school_ids);
		Dto outDto =schoolService.batchDelete(school_idList);
		IMSCxt.write(response, IMSJson.toJson(outDto));
	}
	
}
