package com.ims.business.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.ims.common.core.asset.IMSJson;
import com.ims.business.po.UnitPO;
import com.ims.business.service.UnitService;
import com.ims.common.core.asset.IMSCxt;
import com.ims.common.core.matatype.Dto;
import com.ims.common.core.matatype.Dtos;
import com.ims.common.core.asset.IMSFormater;

/**
 * 
 * 类描述：<b>职能单位信息表[ih_unit]控制类</b>
 * 创建人：陈骑元
 * 邮箱：240823329@qq.com
 * 创建时间：2017-03-07 19:34:50
 * 修改人：
 * 修改时间：
 * 修改备注： 
 * @version 1.0
 */
@Controller
@RequestMapping(value = "business/unit")
public class UnitController {
  
    @Autowired
	private  UnitService unitService;
	
	
	/**
	 * 页面初始化
	 *
	 * @return
	 */
	@RequestMapping(value = "init")
	public ModelAndView init() {
		return new ModelAndView("business/unit/unitList.jsp");
	}
	
	/**
	 * 
	 * 分页查询职能单位信息表信息
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "listUnit")
	public void listUnit(HttpServletRequest request, HttpServletResponse response) {
		Dto pDto = Dtos.newDto(request);
		pDto.setOrder(" unit_type ASC,sort_no ASC ");
		List<UnitPO> unitList =unitService.likePage(pDto);
		String outString = IMSJson.toGridJson(unitList, pDto.getPageTotal());
		IMSCxt.write(response, outString);
	}
	/**
	 * 
	 * 初始化新增职能单位信息表页面
	 * @return
	 * 
	 */
	@RequestMapping(value = "goAdd")
	public ModelAndView goAdd() {
		return new ModelAndView("business/unit/addUnit.jsp");
	}
	/**
	 * 保存职能单位信息表信息
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveUnit")
	public void saveUnit(HttpServletRequest request, HttpServletResponse response) {
	    Dto inDto = Dtos.newDto(request);
		Dto outDto = unitService.save(inDto);
		IMSCxt.write(response, IMSJson.toJson(outDto));
	}
	/**
	 * 查看职能单位信息表详情页面
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "goShow")
	public ModelAndView goShow( String unit_id) {
	   ModelAndView modelAndView= new ModelAndView();
	   UnitPO unitPO=unitService.selectByKey(unit_id);
	   modelAndView.addObject("unitPO",unitPO);
	   modelAndView.setViewName("showUnit.jsp");
	   return modelAndView;
	}
	/**
	 * 初始化修改职能单位信息表页面
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "goModify")
	public ModelAndView goModify( String unit_id) {
	   ModelAndView modelAndView= new ModelAndView();
	   UnitPO unitPO=unitService.selectByKey(unit_id);
	   modelAndView.addObject("unitPO",unitPO);
	   modelAndView.setViewName("business/unit/modifyUnit.jsp");
	   return modelAndView;
	}
	/**
	 * 修改职能单位信息表信息
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "updateUnit")
	public void updateUnit(HttpServletRequest request, HttpServletResponse response) {
		Dto inDto = Dtos.newDto(request);
		Dto outDto =unitService.update(inDto);
		IMSCxt.write(response, IMSJson.toJson(outDto));
	}
	/**
	 * 删除职能单位信息表信息
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteUnit")
	public void deleteUnit( String unit_id,HttpServletResponse response) {
		Dto outDto =unitService.delete(unit_id);
		IMSCxt.write(response, IMSJson.toJson(outDto));
	}
	/**
	 * 批量删除职能单位信息表信息
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "batchDeleteUnit")
	public void batchDeleteUnit( String unit_ids,HttpServletResponse response) {
	    List<String> unit_idList=IMSFormater.separatStringToList(unit_ids);
		Dto outDto =unitService.batchDelete(unit_idList);
		IMSCxt.write(response, IMSJson.toJson(outDto));
	}
	
}
