package com.ims.common.system.modules.controller;
/**
 * 
 * 类名:com.ims.common.system.modules.controller.EmailController
 * 描述:邮件控制类
 * 编写者:陈骑元
 * 创建时间:2017年3月6日 下午6:13:07
 * 修改说明:
 */

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ims.common.core.asset.IMSCxt;
import com.ims.common.core.asset.IMSJson;
import com.ims.common.core.matatype.Dto;
import com.ims.common.core.matatype.Dtos;
import com.ims.common.system.asset.DicCons;
import com.ims.common.system.modules.service.EmailService;
@Controller
@RequestMapping(value = "system/email")
public class EmailController {
	
	@Autowired
	private EmailService emailService;
	
	/**
	 * 页面初始化
	 *
	 * @return
	 */
	@RequestMapping(value = "initEmailRecord")
	public ModelAndView initEmailRecord() {
		return new ModelAndView("system/email/emailRecordList.jsp");
	}
	
	/**
	 * 
	 * 查询邮件记录
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "listEmailRecord")
	public void listEmailRecord(HttpServletRequest request, HttpServletResponse response) {
		Dto pDto = Dtos.newDto(request); 
		pDto.setOrder("a.create_time DESC ");
		List<Dto> emailRecordList=emailService.listEmailRecordPage(pDto);
		String outString = IMSJson.toGridJson(emailRecordList, emailRecordList.size());
		IMSCxt.write(response, outString);
	}
	/**
	 * 
	 *初始化发送邮件界面
	 * @return
	 */
	@RequestMapping(value = "goSendEmail")
	public ModelAndView goSendEmail() {
		return new ModelAndView("system/email/sendEmail.jsp");
	}
	/**
	 * 
	 * 简要说明：保存邮件发送记录
	 * 编写者：陈骑元
	 * 创建时间：2017年3月6日 下午9:01:14
	 * @param 说明
	 * @return 说明
	 */
	@RequestMapping(value = "saveEmailRecord")
	public void saveEmailRecord(HttpServletRequest request, HttpServletResponse response) {
		Dto inDto = Dtos.newDto(request);
		inDto.put("email_type",DicCons.EMAIL_TYPE_NORMAL );
		Dto outDto =emailService.saveEmailRecord(inDto);
		IMSCxt.write(response, IMSJson.toJson(outDto));
	
	}

}
