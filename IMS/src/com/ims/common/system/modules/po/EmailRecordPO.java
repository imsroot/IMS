package com.ims.common.system.modules.po;


import com.ims.common.core.matatype.BasePO;
import java.util.Date;
/**
 * 
 * 类描述： <b>邮件发送记录[sms_email_record]数据持久化对象</b>
 * 创建人：陈骑元
 * 邮箱：240823329@qq.com
 * 创建时间：2017-03-06 17:36:46
 * 修改人：
 * 修改时间：
 * 修改备注： 
 * @version 1.0
 */

public class EmailRecordPO extends BasePO {

	private static final long serialVersionUID = 1L;

	/**
	 * 邮件记录编号
	 */
	private String record_id;
	
	/**
	 * 发件箱编号
	 */
	private String email_id;
	
	/**
	 * 接收邮箱
	 */
	private String receive_email;
	
	/**
	 * 接收人
	 */
	private String receive_name;
	
	/**
	 * 是否发送 0 否 1是
	 */
	private String is_send;
	
	/**
	 * 发送时间
	 */
	private Date send_time;
	
	/**
	 * 状态 0 失败 1 成功
	 */
	private String status;
	

	/**
	 * 邮件记录编号
	 * 
	 * @return record_id
	 */
	public String getRecord_id() {
		return record_id;
	}
	
	/**
	 * 发件箱编号
	 * 
	 * @return email_id
	 */
	public String getEmail_id() {
		return email_id;
	}
	
	/**
	 * 接收邮箱
	 * 
	 * @return receive_email
	 */
	public String getReceive_email() {
		return receive_email;
	}
	
	/**
	 * 接收人
	 * 
	 * @return receive_name
	 */
	public String getReceive_name() {
		return receive_name;
	}
	
	/**
	 * 是否发送 0 否 1是
	 * 
	 * @return is_send
	 */
	public String getIs_send() {
		return is_send;
	}
	
	/**
	 * 发送时间
	 * 
	 * @return send_time
	 */
	public Date getSend_time() {
		return send_time;
	}
	
	/**
	 * 状态 0 失败 1 成功
	 * 
	 * @return status
	 */
	public String getStatus() {
		return status;
	}
	

	/**
	 * 邮件记录编号
	 * 
	 * @param record_id
	 */
	public void setRecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	/**
	 * 发件箱编号
	 * 
	 * @param email_id
	 */
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	
	/**
	 * 接收邮箱
	 * 
	 * @param receive_email
	 */
	public void setReceive_email(String receive_email) {
		this.receive_email = receive_email;
	}
	
	/**
	 * 接收人
	 * 
	 * @param receive_name
	 */
	public void setReceive_name(String receive_name) {
		this.receive_name = receive_name;
	}
	
	/**
	 * 是否发送 0 否 1是
	 * 
	 * @param is_send
	 */
	public void setIs_send(String is_send) {
		this.is_send = is_send;
	}
	
	/**
	 * 发送时间
	 * 
	 * @param send_time
	 */
	public void setSend_time(Date send_time) {
		this.send_time = send_time;
	}
	
	/**
	 * 状态 0 失败 1 成功
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	

}