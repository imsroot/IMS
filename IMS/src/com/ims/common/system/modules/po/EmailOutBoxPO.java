package com.ims.common.system.modules.po;


import com.ims.common.core.matatype.BasePO;
import java.util.Date;
/**
 * 
 * 类描述： <b>邮件发件箱[sms_email_outbox]数据持久化对象</b>
 * 创建人：陈骑元
 * 邮箱：240823329@qq.com
 * 创建时间：2017-03-06 17:22:55
 * 修改人：
 * 修改时间：
 * 修改备注： 
 * @version 1.0
 */

public class EmailOutBoxPO extends BasePO {

	private static final long serialVersionUID = 1L;

	/**
	 * 邮件类型
	 */
	private String email_id;
	
	/**
	 * 邮件主题
	 */
	private String subject;
	
	/**
	 * 邮件内容
	 */
	private String content;
	
	/**
	 * 邮件类型 1 普通邮件 2 验证邮件 3业务邮件 4定时邮件
	 */
	private String email_type;
	
	/**
	 * 创建人
	 */
	private String create_user_id;
	
	/**
	 * 创建时间
	 */
	private Date create_time;
	

	/**
	 * 邮件类型
	 * 
	 * @return email_id
	 */
	public String getEmail_id() {
		return email_id;
	}
	
	/**
	 * 邮件主题
	 * 
	 * @return subject
	 */
	public String getSubject() {
		return subject;
	}
	
	/**
	 * 邮件内容
	 * 
	 * @return content
	 */
	public String getContent() {
		return content;
	}
	
	/**
	 * 邮件类型 1 普通邮件 2 验证邮件 3业务邮件 4定时邮件
	 * 
	 * @return email_type
	 */
	public String getEmail_type() {
		return email_type;
	}
	
	/**
	 * 创建人
	 * 
	 * @return create_user_id
	 */
	public String getCreate_user_id() {
		return create_user_id;
	}
	
	/**
	 * 创建时间
	 * 
	 * @return create_time
	 */
	public Date getCreate_time() {
		return create_time;
	}
	

	/**
	 * 邮件类型
	 * 
	 * @param email_id
	 */
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	
	/**
	 * 邮件主题
	 * 
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	/**
	 * 邮件内容
	 * 
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * 邮件类型 1 普通邮件 2 验证邮件 3业务邮件 4定时邮件
	 * 
	 * @param email_type
	 */
	public void setEmail_type(String email_type) {
		this.email_type = email_type;
	}
	
	/**
	 * 创建人
	 * 
	 * @param create_user_id
	 */
	public void setCreate_user_id(String create_user_id) {
		this.create_user_id = create_user_id;
	}
	
	/**
	 * 创建时间
	 * 
	 * @param create_time
	 */
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	

}