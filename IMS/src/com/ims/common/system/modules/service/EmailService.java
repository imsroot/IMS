package com.ims.common.system.modules.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ims.common.core.asset.IMSCons;
import com.ims.common.core.asset.IMSCxt;
import com.ims.common.core.asset.IMSUtils;
import com.ims.common.core.dao.SqlDao;
import com.ims.common.core.dao.plugin.DBType;
import com.ims.common.core.id.IMSId;
import com.ims.common.core.matatype.Dto;
import com.ims.common.core.matatype.Dtos;
import com.ims.common.system.asset.DicCons;
import com.ims.common.system.modules.po.EmailOutBoxPO;
import com.ims.common.system.modules.po.EmailRecordPO;

@Service("emailService")
public class EmailService {
	@Autowired
	private SqlDao sqlDao;
	
	/**
	 * 
	 * 简要说明：查询待选的用户 编写者：陈骑元 创建时间：2017年1月13日 上午12:04:27
	 * 
	 * @param 说明
	 * @return 说明
	 */
	public List<Dto> listEmailRecordPage(Dto pDto) {

		return sqlDao.list("Email.listEmailRecordPage",pDto);
	}
	/**
	 * 
	 * 简要说明：查询待发送邮件
	 * 编写者：陈骑元
	 * 创建时间：2017年3月7日 下午4:24:09
	 * @param 说明
	 * @return 说明
	 */
	public List<Dto> listSendEmail(Dto pDto) {
		
		return sqlDao.list("Email.listSendEmail",pDto);
	}
	/**
	 * 
	 * 简要说明：保存邮件记录
	 * 编写者：陈骑元
	 * 创建时间：2017年3月6日 下午8:37:48
	 * @param 说明
	 * @return 说明
	 */
	@Transactional
	public boolean insertEmailRecord(Dto inDto){
		
		  String email_id=IMSId.uuid();
		  String currentUserId=inDto.getString(IMSCons.LOGIN_USER_ID);
		  Date current_time=IMSUtils.getDateTime();
		  EmailOutBoxPO outBoxPO=new EmailOutBoxPO();
		  outBoxPO.setEmail_id(email_id);
		  outBoxPO.setContent(inDto.getString("content"));
		  outBoxPO.setSubject(inDto.getString("subject"));
		  outBoxPO.setCreate_time(current_time);
		  outBoxPO.setCreate_user_id(currentUserId);
		  outBoxPO.setEmail_type(inDto.getString("email_type"));
		  EmailRecordPO recordPO=new  EmailRecordPO ();
		  recordPO.setEmail_id(email_id);
		  recordPO.setRecord_id(IMSId.uuid());
		  recordPO.setReceive_email(inDto.getString("receive_email"));
		  recordPO.setReceive_name(inDto.getString("receive_name"));
		  recordPO.setIs_send(IMSCons.IS.NO);
		  recordPO.setStatus(DicCons.EMAIL_STATUS_FAIL);
		  sqlDao.insert("Email.insertEmailOutBox",outBoxPO);
		  int row=sqlDao.insert("Email.insertEmailRecord", recordPO);
		  return row>0;
	}
	/**
	 * 
	 * 简要说明：保存邮件记录信息
	 * 编写者：陈骑元
	 * 创建时间：2017年3月6日 下午8:59:09
	 * @param 说明
	 * @return 说明
	 */
	@Transactional
	public Dto saveEmailRecord(Dto inDto){
		  Dto outDto = Dtos.newDto();
		  boolean result=insertEmailRecord(inDto);
		  if( result){
			   outDto.setAppCode(IMSCons.SUCCESS);
			   outDto.setAppMsg("操作完成，邮件发送信息已经保存到发送列表，请查看发送记录。");
		   }else{
			   outDto.setAppCode(IMSCons.ERROR);
			   outDto.setAppMsg("操作失败，邮件发送信息保存失败。");
		   }
		return outDto;
	}
	/**
	 * 
	 * 简要说明：
	 * 编写者：陈骑元
	 * 创建时间：2017年3月7日 下午4:20:23
	 * @param 说明
	 * @return 说明
	 */
	public boolean  updateEmailRecord(EmailRecordPO emailRecordPO){
		 int row=sqlDao.insert("Email.updateEmailRecordByKey", emailRecordPO);
		 return row>0;
	}
}
