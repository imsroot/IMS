package com.ims.common.system.quartz;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.ims.common.core.asset.IMSCons;
import com.ims.common.core.asset.IMSCxt;
import com.ims.common.core.asset.IMSUtils;
import com.ims.common.core.matatype.Dto;
import com.ims.common.core.matatype.Dtos;
import com.ims.common.core.support.mail.Email;
import com.ims.common.core.support.mail.EmailSender;
import com.ims.common.system.asset.DicCons;
import com.ims.common.system.modules.po.EmailRecordPO;
import com.ims.common.system.modules.service.EmailService;


/**
 * 
 * 类名:com.ims.common.system.quartz.SystemJob
 * 描述:系统发送任务
 * 编写者:陈骑元
 * 创建时间:2017年3月6日 下午9:18:06
 * 修改说明:
 */

public class SystemJob {
	
    private static final Logger log = Logger.getLogger(SystemJob.class);
	@Autowired
	private EmailService emailService;
	
	/**
	 * 
	 * 简要说明：每十秒发送一次邮件
	 * 编写者：陈骑元
	 * 创建时间：2017年3月6日 下午9:21:10
	 * @param 说明
	 * @return 说明
	 */
    public void sendEmail(){
    	String email_switch=IMSCxt.getParamValue("email_switch");
    	if(IMSCons.SWITCH_ON.equals(email_switch)){
    		Dto pDto=Dtos.newDto();
    		pDto.put("now_time", IMSUtils.getDateTimeStr());
    		List<Dto> sendEmailList=emailService.listSendEmail(pDto);
    	    for(int i=0;i<sendEmailList.size();i++){
    	    	Dto emailList=sendEmailList.get(i);
    	    	Email params=new Email();
    			params.setEmailSendTo(emailList.getString("receive_email"));
    			params.setEmailSendSubject(emailList.getString("subject"));
    			params.setEmailSendContent(emailList.getString("content"));
    			boolean result=EmailSender.send(params);
    			String status=DicCons.EMAIL_STATUS_FAIL;
    			if(result){
    				 status=DicCons.EMAIL_STATUS_SUCCESS;
    			}
    			EmailRecordPO emailRecordPO=new EmailRecordPO();
    			emailRecordPO.setStatus(status);
    			emailRecordPO.setIs_send(IMSCons.IS.YES);
    			emailRecordPO.setSend_time(IMSUtils.getDateTime());
    		    emailRecordPO.setRecord_id(emailList.getString("record_id"));
    			emailService.updateEmailRecord(emailRecordPO);
    	    }
    	}else{
    		log.info("邮件发送开关已经关闭，如果要开启请进行键值参数配置把email_switch键值设置为on");
    	}
    	
    }

}
