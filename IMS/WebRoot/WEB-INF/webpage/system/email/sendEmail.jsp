<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/webpage/common/taglibs.jsp"%>
<head>
<meta charset="UTF-8">
</head>
<body style="margin: 0; padding: 0">
	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'center',border:false" style="padding: 5px;">
			<form id="sendEmailForm" action="${ctx }/system/email/saveEmailRecord.jhtml"
				method="post" >
			
				<table cellpadding=5 cellspacing=0 width=100% align="center" class="formTabel">
				
					<tr>
						<td align="right" width="100px">收件人：</td>
						<td><input  type="text"  name="receive_name" required="true"
							class="easyui-textbox" data-options="validType:'length[1,50]'"
							style="width: 420px; height: 30px" ></td>
					</tr>
					
					
				  <tr>
						<td align="right" width="100px">收件人邮箱：</td>
						<td><input name="receive_email" required="true"
							class="easyui-textbox" validType="email"
							style="width: 420px; height: 30px" ></td>
					</tr>
					
				  <tr>
						<td align="right" width="100px">主题：</td>
						<td><input name="subject" type="text"   required="true"
							class="easyui-textbox" data-options="validType:'length[1,100]'" 
							style="width: 420px; height: 30px" ></td>
					</tr>
				
					
					<tr>
						<td align="right" width="100px">内容：</td>
						<td><input name="content" type="text"  required="true" class="easyui-textbox"
							data-options="multiline:true,validType:'length[1,4000]'"
							style="width: 420px; height: 220px"></td>
					</tr>
				
					
				

				</table>

			</form>
		</div>
		<div data-options="region:'south',border:false" height="35px"
			style="text-align: center; background: #F4F4F4; padding: 5px 0 0;">
			<a class="easyui-linkbutton" data-options="iconCls:'ok'"href="javascript:void(0)"
			   onclick="submitFormData('sendEmailForm','emailRecordList','sendEmailWindow')" style="width: 70px">保存</a> &nbsp;
		    <a class="easyui-linkbutton" data-options="iconCls:'close'" href="javascript:void(0)"
				onclick="closeWindow('sendEmailWindow')" style="width: 70px">关闭</a>
		</div>
	</div>

</body>
</html>