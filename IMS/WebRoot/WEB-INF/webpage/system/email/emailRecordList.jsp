<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/webpage/common/taglibs.jsp"%>
<head>
<meta charset="UTF-8">
<IMS:codeStore fields="is_send,email_type,email_status"/>
<IMS:codeFormatter fields="is_send,email_type,email_status"/>
</head>
<script type="text/javascript">
</script>
<body style="margin: 0; padding: 0" >
<div class="easyui-layout"  data-options="fit:true">
<div  style="height:67px;background-color: white;"  data-options="region:'north',split:false">
        <form id="queryForm" method="post">
        <table class="searchContent">
					<tr>
						<td width="7%" style="text-align:right">
							收件人：
						</td>
						<td width="13%" style="text-align:left">
							<input type="text" name="receive_name" class="easyui-textbox" style="width: 140px;"/>
						</td>
						<td width="7%" style="text-align:right">
							收件人邮箱：
						</td>
						<td width="13%" style="text-align:left">
							<input type="text" name="receive_email" class="easyui-textbox" style="width: 140px;"/>
						</td>
						<td width="7%" style="text-align:right">
							主题：
						</td>
						<td width="13%" style="text-align:left">
							<input type="text" name="subject" class="easyui-textbox" style="width: 140px;"/>
						</td>
						<td width="7%" style="text-align:right">
							内容：
						</td>
						<td width="18%" style="text-align:left">
							<input type="text" name="content" class="easyui-textbox" style="width: 210px;"/>
						</td>
						
					  
						<td width="15%" rowspan="4" algin="center">
							<div>
								&nbsp;<a href="javascript:void(0)" class="easyui-linkbutton"
									iconCls="icon-search" onclick="doQuery('emailRecordList','queryForm')">查询</a> &nbsp;
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="refresh" 
							onclick="$('#queryForm').form('reset')">重置</a> 
							</div>
						</td>
					</tr>
					<tr>
						<td width="7%" style="text-align:right">
							邮件类型：
						</td>
						<td width="13%" style="text-align:left">
							<input type="text" name="email_type" class="easyui-combobox" editable="false"  data-options="data:email_typeStore,textField:1,valueField:0" style="width: 140px;"/>
						</td>
						<td width="7%" style="text-align:right">
							发送状态：
						</td>
						<td width="13%" style="text-align:left">
							<input type="text" name="is_send" class="easyui-combobox" editable="false"  data-options="data:is_sendStore,textField:1,valueField:0" style="width: 140px;"/>
						</td>
						<td width="7%" style="text-align:right">
							投递状态：
						</td>
						<td width="13%" style="text-align:left">
							<input type="text" name="email_status" class="easyui-combobox" editable="false"  data-options="data:email_statusStore,textField:1,valueField:0" style="width: 140px;"/>
						</td>
						<td width="7%" style="text-align:right">
							创建日期：
						</td>
						<td width="18%" style="text-align:left">
								<input type="text"  name="carete_time_begin" class="easyui-datebox" editable="false" style="width:100px" />
							至<input type="text" name="carete_time__end"  class="easyui-datebox" editable="false" style="width:100px" />
						</td>
						
					  
						
					</tr>
					
				</table>
        </form>
      </div>
    <div id="main" data-options="region:'center',split:false, border:false">	
	<table id="emailRecordList" class="easyui-datagrid" style="width: 100%; height: 100%; padding: 0px;"
		data-options="
				     border:false,
	                rownumbers:true,
	                singleSelect:true,
	                autoRowHeight:false, 
	                pagination:true,
	                toolbar:'#toolbar',
	                striped:true,
	                queryParams : $('#queryForm').serializeObject(),
	                fit:true,
	                url:'${ctx}/system/email/listEmailRecord.jhtml',
	                pageSize:20">
	               
		<thead>
			<tr>
				<th field="record_id" hidden="true" align="center">角色编号</th>
				<th field="receive_name" formatter="formatCellTooltip" width="14%" align="center">收件人</th>
				<th field="receive_email" formatter="formatCellTooltip" width="14%" align="center">收件人邮箱</th>
				<th field="subject" formatter="formatCellTooltip"   width="17%" align="center">主题</th>
				<th field="content"  formatter="formatCellTooltip"   width="23%" align="center">内容</th>
				<th field="email_type" formatter="email_typeFormatter"   width="6%" align="center">邮件类型</th>
				<th field="is_send"   formatter="is_sendFormatter"  width="6%" align="center">是否发送</th>
				<th field="status"    formatter="email_statusFormatter"  width="6%" align="center">投递状态</th>
				<th field="create_time"     width="12%" align="center">创建时间</th>
				
			</tr>
		</thead>
	</table>
	<div id="toolbar" style="padding: 2px;">
        
		<a href="#" class="easyui-linkbutton" iconCls="email_go"
			plain="true"
			onclick="showWindow('sendEmailWindow','${ctx}/system/email/goSendEmail.jhtml');">发送邮件</a> 
		
	  
	</div>

    </div>
    </div>
    <div id="sendEmailWindow" class="easyui-window" title="发送邮件"
		data-options="collapsible:false,shadow:false,minimizable:false,maximizable:false,modal:true,closed:true"
		style="width: 650px; height:440px; background-color: #FFFFFF"></div>
  
   
   
    
</body>


