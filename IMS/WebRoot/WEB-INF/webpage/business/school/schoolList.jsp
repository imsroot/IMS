<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/webpage/common/taglibs.jsp"%>
<head>
<meta charset="UTF-8">
<IMS:codeStore fields="school_type"/>
<IMS:codeFormatter fields="street_type,school_type" />
<IMS:codeStoreFilter filter="0" field="street_type"/>
</head>
<body style="margin: 0; padding: 0">
	<div class="easyui-layout" data-options="fit:true">
		<div style="height: 37px; background-color: white;"
			data-options="region:'north',split:false">
			<form id="queryForm" method="post">
				<table class="searchContent">
					<tr>
						<td width="7%" style="text-align: right">街镇：</td>
						<td width="13%" style="text-align: left">
						<input type="text"
							name="street" class="easyui-combobox" editable="false"  data-options="data:street_typeStore,textField:1,valueField:0" style="width: 200px;" />
						</td>
						<td width="7%" style="text-align: right">学校名称：</td>
						<td width="13%" style="text-align: left"><input type="text"
							name="school_name" class="easyui-textbox" style="width: 200px;" />
						</td>
						<td width="7%" style="text-align: right">学校类型：</td>
						<td width="13%" style="text-align: left"><input type="text"
							name="school_type" class="easyui-combobox" editable="false"  data-options="data:school_typeStore,textField:1,valueField:0" style="width: 200px;" /></td>

						<td width="35%" rowspan="4" algin="center">
							<div>
								&nbsp;<a href="javascript:void(0)" class="easyui-linkbutton"
									iconCls="icon-search"
									onclick="doQuery('schoolList','queryForm')">查询</a> &nbsp;
								<a href="javascript:void(0)" class="easyui-linkbutton"
									iconCls="refresh" onclick="$('#queryForm').form('reset')">重置</a>
							</div>
						</td>
					</tr>
					

				</table>
			</form>
		</div>
		<div id="main"
			data-options="region:'center',split:false, border:false">
			<table id="schoolList" class="easyui-datagrid"
				style="width: 100%; height: 100%; padding: 0px;"
				data-options="
				     border:false,
	                rownumbers:true,
	                singleSelect:false,
	                autoRowHeight:false, 
	                pagination:true,
	                toolbar:'#toolbar',
	                striped:true,
	                queryParams : $('#queryForm').serializeObject(),
	                fit:true,
	                url:'${ctx}/business/school/listSchool.jhtml',
	                pageSize:20">

				<thead>
					<tr>
						<th field="school_id" hidden="true" align="center">单位编号</th>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="street" formatter="street_typeFormatter" width="9%"
							align="center">街镇</th>
						<th field="school_name" formatter="formatCellTooltip" width="25%"
							align="center">学校名称</th>
						<th field="places" formatter="formatCellTooltip" width="10%"
							align="center">名额</th>
						<th field="school_address" formatter="formatCellTooltip" width="30%"
							align="center">地址</th>
						<th field="school_type" formatter="school_typeFormatter" width="8%" align="center">学校类型</th>
						<th field="create_time"  width="13%" align="center">创建时间</th>
						

					</tr>
				</thead>
			</table>
			<div id="toolbar" style="padding: 2px;">

				<a href="#" class="easyui-linkbutton" iconCls="add" plain="true"
					onclick="showWindow('addSchoolWindow','${ctx}/business/school/goAdd.jhtml');">新增</a>

				<a href="javascript:void(0);" class="easyui-linkbutton"
					iconCls="edit" plain="true" onclick="modifyGridData('modifySchoolWindow','schoolList','school_id','${ctx}/business/school/goModify.jhtml','请选择你要修改的学校信息');"">修改</a> <a
					href="#" class="easyui-linkbutton" iconCls="del" plain="true"
					onclick="batchDeleteGridData('schoolList','school_id','${ctx}/business/school/batchDeleteSchool.jhtml','请选择你删除的学校信息','你确认要删除选择的学校信息吗');">删除</a>


			</div>

		</div>
	</div>
  <div id="addSchoolWindow" class="easyui-window" title="新增学校"
		data-options="collapsible:false,shadow:false,minimizable:false,maximizable:false,modal:true,closed:true"
		style="width: 500px; height: 370px; background-color: #FFFFFF"></div>
  <div id="modifySchoolWindow" class="easyui-window" title="修改学校"
		data-options="collapsible:false,shadow:false,minimizable:false,maximizable:false,modal:true,closed:true"
		style="width: 500px; height: 370px; background-color: #FFFFFF"></div>



</body>


