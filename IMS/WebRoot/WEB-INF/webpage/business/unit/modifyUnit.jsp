<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/webpage/common/taglibs.jsp"%>
<head>
<meta charset="UTF-8">
</head>
<body style="margin: 0; padding: 0">
	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'center',border:false" style="padding: 5px;">
			<form id="modifyUnitForm" action="${ctx }/business/unit/updateUnit.jhtml"
				method="post">
          <input type="hidden"  name="unit_id" value="${unitPO.unit_id}" />
				<table cellpadding=5 cellspacing=0 width=100% align="center"
					class="formTabel">

					<tr>
						<td align="right" width="100px">单位名称：</td>
						<td><input type="text" name="unit_name" value="${unitPO.unit_name }" required="true"
							class="easyui-textbox" data-options="validType:'length[1,100]'"
							style="width: 280px; height: 30px"></td>
					</tr>


					<tr>
						<td align="right" width="100px">服务范围：</td>
						<td><input name="unit_serve" value="${unitPO.unit_serve }" required="true"  multiple="true"
							editable="false" class="easyui-combobox"
							data-options="data:street_typeStore,textField:1,valueField:0,icons:[{
						iconCls:'icon-remove',
						 handler: function(e){
					                    $(e.data.target).combobox('clear')
				           }
					}]"
							style="width: 280px; height: 30px"></td>
					</tr>

					<tr>
						<td align="right" width="100px">地址：</td>
						<td><input name="unit_address" value="${unitPO.unit_address}" type="text" required="true"
							class="easyui-textbox" data-options="validType:'length[1,200]'"
							style="width: 280px; height: 30px"></td>
					</tr>
					<tr>
						<td align="right" width="100px">电话：</td>
						<td><input name="unit_phone" value="${unitPO.unit_phone }" type="text" required="true"
							class="easyui-textbox" data-options="validType:'length[1,100]'"
							style="width: 280px; height: 30px"></td>
					</tr>
					<tr>
						<td align="right" width="100px">单位类型：</td>
						<td><input type="text" name="unit_type" value="${unitPO.unit_type }"
							class="easyui-combobox" editable="false" required="true" value="1"
							data-options="data:unit_typeStore,textField:1,valueField:0"
							style="width: 280px; height: 30px"/></td>
					</tr>
					<tr>
						<td align="right" width="100px">排序号：</td>
						<td><input name="sort_no" type="text" value="${unitPO.sort_no }"
							class="easyui-numberspinner" value="1" data-options="min:1,max:1000000,required:true"
							style="width: 280px; height: 30px" required="true"></td>
					</tr>
                    <tr>
						<td align="right" width="100px">备注：</td>
						<td colspan="3"><input name="remark" value="${unitPO.remark }" type="text"  class="easyui-textbox"
							data-options="multiline:true,validType:'length[0,200]'"
							style="width: 280px; height: 70px"></td>
					</tr>





				</table>

			</form>
		</div>
		<div data-options="region:'south',border:false" height="35px"
			style="text-align: center; background: #F4F4F4; padding: 5px 0 0;">
			<a class="easyui-linkbutton" data-options="iconCls:'ok'"
				href="javascript:void(0)"
				onclick="submitFormData('modifyUnitForm','unitList','modifyUnitWindow')"
				style="width: 70px">保存</a> &nbsp; <a class="easyui-linkbutton"
				data-options="iconCls:'close'" href="javascript:void(0)"
				onclick="closeWindow('modifyUnitWindow')" style="width: 70px">关闭</a>
		</div>
	</div>

</body>
</html>