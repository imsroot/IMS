<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/webpage/common/taglibs.jsp"%>
<head>
<meta charset="UTF-8">
<IMS:codeStore fields="unit_type,street_type" />
<IMS:codeFormatter fields="unit_type" />
<IMS:multiCodeFormatter fields="street_type"/>
</head>
<body style="margin: 0; padding: 0">
	<div class="easyui-layout" data-options="fit:true">
		<div style="height: 37px; background-color: white;"
			data-options="region:'north',split:false">
			<form id="queryForm" method="post">
				<table class="searchContent">
					<tr>
						<td width="7%" style="text-align: right">单位名称：</td>
						<td width="13%" style="text-align: left"><input type="text"
							name="unit_name" class="easyui-textbox" style="width: 200px;" />
						</td>
						<td width="7%" style="text-align: right">地址：</td>
						<td width="13%" style="text-align: left"><input type="text"
							name="unit_address" class="easyui-textbox" style="width: 200px;" />
						</td>
						<td width="7%" style="text-align: right">单位类型：</td>
						<td width="13%" style="text-align: left"><input type="text"
							name="unit_type" class="easyui-combobox" editable="false"  data-options="data:unit_typeStore,textField:1,valueField:0" style="width: 200px;" /></td>
						


						<td width="35%" rowspan="4" algin="center">
							<div>
								&nbsp;<a href="javascript:void(0)" class="easyui-linkbutton"
									iconCls="icon-search"
									onclick="doQuery('unitList','queryForm')">查询</a> &nbsp;
								<a href="javascript:void(0)" class="easyui-linkbutton"
									iconCls="refresh" onclick="$('#queryForm').form('reset')">重置</a>
							</div>
						</td>
					</tr>
					

				</table>
			</form>
		</div>
		<div id="main"
			data-options="region:'center',split:false, border:false">
			<table id="unitList" class="easyui-datagrid"
				style="width: 100%; height: 100%; padding: 0px;"
				data-options="
				     border:false,
	                rownumbers:true,
	                singleSelect:false,
	                autoRowHeight:false, 
	                pagination:true,
	                toolbar:'#toolbar',
	                striped:true,
	                queryParams : $('#queryForm').serializeObject(),
	                fit:true,
	                url:'${ctx}/business/unit/listUnit.jhtml',
	                pageSize:20">

				<thead>
					<tr>
						<th field="unit_id" hidden="true" align="center">单位编号</th>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="unit_name" formatter="formatCellTooltip" width="15%"
							align="center">单位名称</th>
						<th field="unit_serve" formatter="street_typeFormatter" width="20%"
							align="center">服务范围</th>
						<th field="unit_address" formatter="formatCellTooltip" width="28%"
							align="center">地址</th>
						<th field="unit_phone" formatter="formatCellTooltip" width="15%"
							align="center">电话</th>
						<th field="unit_type" formatter="unit_typeFormatter" width="12%" align="center">单位类型</th>
						<th field="sort_no" width="7%" align="center">排序号</th>

					</tr>
				</thead>
			</table>
			<div id="toolbar" style="padding: 2px;">

				<a href="#" class="easyui-linkbutton" iconCls="add" plain="true"
					onclick="showWindow('addUnitWindow','${ctx}/business/unit/goAdd.jhtml');">新增</a>

				<a href="javascript:void(0);" class="easyui-linkbutton"
					iconCls="edit" plain="true" onclick="modifyGridData('modifyUnitWindow','unitList','unit_id','${ctx}/business/unit/goModify.jhtml','请选择你要修改的单位信息');"">修改</a> <a
					href="#" class="easyui-linkbutton" iconCls="del" plain="true"
					onclick="batchDeleteGridData('unitList','unit_id','${ctx}/business/unit/batchDeleteUnit.jhtml','请选择你删除的单位信息','你确认要删除选择的单位信息吗');">删除</a>


			</div>

		</div>
	</div>
  <div id="addUnitWindow" class="easyui-window" title="新增单位"
		data-options="collapsible:false,shadow:false,minimizable:false,maximizable:false,modal:true,closed:true"
		style="width: 500px; height: 410px; background-color: #FFFFFF"></div>
  <div id="modifyUnitWindow" class="easyui-window" title="修改单位"
		data-options="collapsible:false,shadow:false,minimizable:false,maximizable:false,modal:true,closed:true"
		style="width: 500px; height: 410px; background-color: #FFFFFF"></div>



</body>


