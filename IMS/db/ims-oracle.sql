prompt PL/SQL Developer import file
prompt Created on 2017年5月3日 by 蓝枫
set feedback off
set define off
prompt Creating IH_SCHOOL...
create table IH_SCHOOL
(
  school_id      VARCHAR2(50) not null,
  street         VARCHAR2(10),
  school_name    VARCHAR2(100),
  places         NUMBER(10),
  school_address VARCHAR2(200),
  remark         VARCHAR2(1000),
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50),
  school_type    VARCHAR2(10)
)
;
comment on table IH_SCHOOL
  is '学校信息表';
comment on column IH_SCHOOL.school_id
  is '学校编号';
comment on column IH_SCHOOL.street
  is '所属街镇';
comment on column IH_SCHOOL.school_name
  is '学校名称';
comment on column IH_SCHOOL.places
  is '名额';
comment on column IH_SCHOOL.school_address
  is '学校地址';
comment on column IH_SCHOOL.remark
  is '备注';
comment on column IH_SCHOOL.create_time
  is '创建时间';
comment on column IH_SCHOOL.create_user_id
  is '创建用户编号';
comment on column IH_SCHOOL.modify_time
  is '修改时间';
comment on column IH_SCHOOL.modify_user_id
  is '修改用户id';
comment on column IH_SCHOOL.school_type
  is '学校类型';
alter table IH_SCHOOL
  add constraint PK_SCHOOL_ID primary key (SCHOOL_ID);

prompt Creating IH_UNIT...
create table IH_UNIT
(
  unit_id        VARCHAR2(50) not null,
  unit_name      VARCHAR2(100),
  unit_serve     VARCHAR2(50),
  unit_type      VARCHAR2(10),
  unit_address   VARCHAR2(200),
  unit_phone     VARCHAR2(100),
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50),
  sort_no        NUMBER(10),
  remark         VARCHAR2(200)
)
;
comment on table IH_UNIT
  is '职能单位信息表';
comment on column IH_UNIT.unit_id
  is '单位编号主键';
comment on column IH_UNIT.unit_name
  is '单位名称';
comment on column IH_UNIT.unit_serve
  is '服务范围';
comment on column IH_UNIT.unit_type
  is '单位类型';
comment on column IH_UNIT.unit_address
  is '单位地址';
comment on column IH_UNIT.unit_phone
  is '单位电话';
comment on column IH_UNIT.create_time
  is '创建时间';
comment on column IH_UNIT.create_user_id
  is '创建用户';
comment on column IH_UNIT.modify_time
  is '修改用户';
comment on column IH_UNIT.modify_user_id
  is '修改用户编号';
comment on column IH_UNIT.sort_no
  is '排序';
comment on column IH_UNIT.remark
  is '备注';
alter table IH_UNIT
  add constraint PK_UNIT_ID primary key (UNIT_ID);

prompt Creating SMS_EMAIL_OUTBOX...
create table SMS_EMAIL_OUTBOX
(
  email_id       VARCHAR2(50) not null,
  subject        VARCHAR2(100),
  content        VARCHAR2(4000),
  email_type     VARCHAR2(10) default 1,
  create_user_id VARCHAR2(50),
  create_time    DATE
)
;
comment on table SMS_EMAIL_OUTBOX
  is '邮件发件箱';
comment on column SMS_EMAIL_OUTBOX.email_id
  is '邮件类型';
comment on column SMS_EMAIL_OUTBOX.subject
  is '邮件主题';
comment on column SMS_EMAIL_OUTBOX.content
  is '邮件内容';
comment on column SMS_EMAIL_OUTBOX.email_type
  is '邮件类型 1 普通邮件 2 验证邮件 3业务邮件 4定时邮件';
comment on column SMS_EMAIL_OUTBOX.create_user_id
  is '创建人';
comment on column SMS_EMAIL_OUTBOX.create_time
  is '创建时间';
alter table SMS_EMAIL_OUTBOX
  add constraint PK_EMAIL_ID primary key (EMAIL_ID);

prompt Creating SMS_EMAIL_RECORD...
create table SMS_EMAIL_RECORD
(
  record_id     VARCHAR2(50) not null,
  email_id      VARCHAR2(50) not null,
  receive_email VARCHAR2(50),
  receive_name  VARCHAR2(50),
  is_send       VARCHAR2(10) default 0,
  send_time     DATE,
  status        VARCHAR2(10) default 0
)
;
comment on table SMS_EMAIL_RECORD
  is '邮件发送记录';
comment on column SMS_EMAIL_RECORD.record_id
  is '邮件记录编号';
comment on column SMS_EMAIL_RECORD.email_id
  is '发件箱编号';
comment on column SMS_EMAIL_RECORD.receive_email
  is '接收邮箱';
comment on column SMS_EMAIL_RECORD.receive_name
  is '接收人';
comment on column SMS_EMAIL_RECORD.is_send
  is '是否发送 0 否 1是';
comment on column SMS_EMAIL_RECORD.send_time
  is '发送时间';
comment on column SMS_EMAIL_RECORD.status
  is '状态 0 失败 1 成功';
alter table SMS_EMAIL_RECORD
  add constraint PK_SMS_RECORD_ID primary key (RECORD_ID);

prompt Creating SYS_CATALOG...
create table SYS_CATALOG
(
  catalog_id     VARCHAR2(50) not null,
  cascade_id     VARCHAR2(500),
  root_key       VARCHAR2(100),
  root_name      VARCHAR2(100),
  catalog_name   VARCHAR2(50),
  parent_id      VARCHAR2(50),
  sort_no        NUMBER(10),
  icon_name      VARCHAR2(50),
  is_auto_expand VARCHAR2(10),
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50)
)
;
comment on table SYS_CATALOG
  is '分类科目';
comment on column SYS_CATALOG.catalog_id
  is '分类科目编号';
comment on column SYS_CATALOG.cascade_id
  is '分类科目语义ID';
comment on column SYS_CATALOG.root_key
  is '科目标识键';
comment on column SYS_CATALOG.root_name
  is '科目名称';
comment on column SYS_CATALOG.catalog_name
  is '分类名称';
comment on column SYS_CATALOG.parent_id
  is '父节点编号';
comment on column SYS_CATALOG.sort_no
  is '排序号';
comment on column SYS_CATALOG.icon_name
  is '图标名称';
comment on column SYS_CATALOG.is_auto_expand
  is '是否自动展开';
comment on column SYS_CATALOG.create_time
  is '创建时间';
comment on column SYS_CATALOG.create_user_id
  is '创建用户ID';
comment on column SYS_CATALOG.modify_time
  is '修改时间';
comment on column SYS_CATALOG.modify_user_id
  is '修改用户ID';
alter table SYS_CATALOG
  add constraint PK_CATALOG_ID primary key (CATALOG_ID);

prompt Creating SYS_DEPT...
create table SYS_DEPT
(
  dept_id        VARCHAR2(50) not null,
  cascade_id     VARCHAR2(255) not null,
  dept_name      VARCHAR2(100) not null,
  parent_id      VARCHAR2(50) not null,
  dept_code      VARCHAR2(50),
  manager        VARCHAR2(50),
  phone          VARCHAR2(50),
  fax            VARCHAR2(50),
  address        VARCHAR2(200),
  is_auto_expand VARCHAR2(10),
  icon_name      VARCHAR2(50),
  sort_no        NUMBER(10),
  remark         VARCHAR2(400),
  is_del         VARCHAR2(10) default 0,
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50)
)
;
comment on table SYS_DEPT
  is '组织机构';
comment on column SYS_DEPT.dept_id
  is '流水号';
comment on column SYS_DEPT.cascade_id
  is '节点语义ID';
comment on column SYS_DEPT.dept_name
  is '组织名称';
comment on column SYS_DEPT.parent_id
  is '父节点流水号';
comment on column SYS_DEPT.dept_code
  is '机构代码';
comment on column SYS_DEPT.manager
  is '主要负责人';
comment on column SYS_DEPT.phone
  is '部门电话';
comment on column SYS_DEPT.fax
  is '传真';
comment on column SYS_DEPT.address
  is '地址';
comment on column SYS_DEPT.is_auto_expand
  is '是否自动展开';
comment on column SYS_DEPT.icon_name
  is '节点图标文件名称';
comment on column SYS_DEPT.sort_no
  is '排序号';
comment on column SYS_DEPT.remark
  is '备注';
comment on column SYS_DEPT.is_del
  is '是否已删除 0有效 1删除';
comment on column SYS_DEPT.create_time
  is '创建时间';
comment on column SYS_DEPT.create_user_id
  is '创建人ID';
comment on column SYS_DEPT.modify_time
  is '修改时间';
comment on column SYS_DEPT.modify_user_id
  is '修改用户ID';
alter table SYS_DEPT
  add constraint PK_DEPT_ID primary key (DEPT_ID);

prompt Creating SYS_DICTIONARY...
create table SYS_DICTIONARY
(
  dic_id         VARCHAR2(50) not null,
  dic_index_id   VARCHAR2(255),
  dic_code       VARCHAR2(100),
  dic_value      VARCHAR2(100),
  show_color     VARCHAR2(50),
  status         VARCHAR2(10) default 1,
  edit_mode      VARCHAR2(10) default 1,
  sort_no        NUMBER(10),
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50)
)
;
comment on table SYS_DICTIONARY
  is '数据字典';
comment on column SYS_DICTIONARY.dic_id
  is '字典编号';
comment on column SYS_DICTIONARY.dic_index_id
  is '所属字典流水号';
comment on column SYS_DICTIONARY.dic_code
  is '字典对照码';
comment on column SYS_DICTIONARY.dic_value
  is '字典对照值';
comment on column SYS_DICTIONARY.show_color
  is '显示颜色';
comment on column SYS_DICTIONARY.status
  is '当前状态(0:停用;1:启用)';
comment on column SYS_DICTIONARY.edit_mode
  is '编辑模式(0:只读;1:可编辑)';
comment on column SYS_DICTIONARY.sort_no
  is '排序号';
comment on column SYS_DICTIONARY.create_time
  is '创建时间';
comment on column SYS_DICTIONARY.create_user_id
  is '创建用户编号';
comment on column SYS_DICTIONARY.modify_time
  is '修改时间';
comment on column SYS_DICTIONARY.modify_user_id
  is '修改用户ID';
alter table SYS_DICTIONARY
  add constraint PK_DIC_ID primary key (DIC_ID);

prompt Creating SYS_DICTIONARY_INDEX...
create table SYS_DICTIONARY_INDEX
(
  dic_index_id       VARCHAR2(50) not null,
  dic_key            VARCHAR2(50),
  dic_name           VARCHAR2(100),
  catalog_id         VARCHAR2(50),
  catalog_cascade_id VARCHAR2(500),
  dic_remark         VARCHAR2(500),
  create_time        DATE,
  create_user_id     VARCHAR2(50),
  modify_time        DATE,
  modify_user_id     VARCHAR2(50)
)
;
comment on table SYS_DICTIONARY_INDEX
  is '数据字单类型';
comment on column SYS_DICTIONARY_INDEX.dic_index_id
  is '流水号';
comment on column SYS_DICTIONARY_INDEX.dic_key
  is '字典标识';
comment on column SYS_DICTIONARY_INDEX.dic_name
  is '字典名称';
comment on column SYS_DICTIONARY_INDEX.catalog_id
  is '所属分类流水号';
comment on column SYS_DICTIONARY_INDEX.catalog_cascade_id
  is '所属分类流节点语义ID';
comment on column SYS_DICTIONARY_INDEX.dic_remark
  is '备注';
comment on column SYS_DICTIONARY_INDEX.create_time
  is '创建时间';
comment on column SYS_DICTIONARY_INDEX.create_user_id
  is '创建用户编号';
comment on column SYS_DICTIONARY_INDEX.modify_time
  is '修改时间';
comment on column SYS_DICTIONARY_INDEX.modify_user_id
  is '修改用户ID';
alter table SYS_DICTIONARY_INDEX
  add constraint PK_DIC_INDEX_ID primary key (DIC_INDEX_ID);

prompt Creating SYS_MENU...
create table SYS_MENU
(
  menu_id        VARCHAR2(50) not null,
  cascade_id     VARCHAR2(500),
  menu_name      VARCHAR2(100),
  parent_id      VARCHAR2(50),
  icon_name      VARCHAR2(50),
  is_auto_expand VARCHAR2(10) default 0,
  url            VARCHAR2(100),
  remark         VARCHAR2(500),
  status         VARCHAR2(10) default 1,
  edit_mode      VARCHAR2(10) default 1,
  sort_no        NUMBER(10),
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50),
  menu_type      VARCHAR2(10) default 1
)
;
comment on table SYS_MENU
  is '菜单配置';
comment on column SYS_MENU.menu_id
  is '菜单编号';
comment on column SYS_MENU.cascade_id
  is '分类科目语义ID';
comment on column SYS_MENU.menu_name
  is '菜单名称';
comment on column SYS_MENU.parent_id
  is '菜单父级编号';
comment on column SYS_MENU.icon_name
  is '图标名称';
comment on column SYS_MENU.is_auto_expand
  is '是否自动展开';
comment on column SYS_MENU.url
  is 'url地址';
comment on column SYS_MENU.remark
  is '备注';
comment on column SYS_MENU.status
  is '当前状态(0:停用;1:启用)';
comment on column SYS_MENU.edit_mode
  is '编辑模式(0:只读;1:可编辑)';
comment on column SYS_MENU.sort_no
  is '排序号';
comment on column SYS_MENU.create_time
  is '创建时间';
comment on column SYS_MENU.create_user_id
  is '创建用户编号';
comment on column SYS_MENU.modify_time
  is '修改时间';
comment on column SYS_MENU.modify_user_id
  is '修改用户ID';
comment on column SYS_MENU.menu_type
  is '菜单类型 1缺省';
alter table SYS_MENU
  add constraint PK_MENU primary key (MENU_ID);

prompt Creating SYS_PARAM...
create table SYS_PARAM
(
  param_id           VARCHAR2(50) not null,
  param_name         VARCHAR2(100),
  param_key          VARCHAR2(50),
  param_value        VARCHAR2(500),
  catalog_id         VARCHAR2(50),
  catalog_cascade_id VARCHAR2(500),
  param_remark       VARCHAR2(200),
  status             VARCHAR2(10) default 1,
  edit_mode          VARCHAR2(10) default 1,
  create_time        DATE,
  create_user_id     VARCHAR2(50),
  modify_time        DATE,
  modify_user_id     VARCHAR2(50)
)
;
comment on table SYS_PARAM
  is '键值参数';
comment on column SYS_PARAM.param_id
  is '参数编号';
comment on column SYS_PARAM.param_name
  is '参数名称';
comment on column SYS_PARAM.param_key
  is '参数键名';
comment on column SYS_PARAM.param_value
  is '参数键值';
comment on column SYS_PARAM.catalog_id
  is '目录ID';
comment on column SYS_PARAM.catalog_cascade_id
  is '分类科目语义ID';
comment on column SYS_PARAM.param_remark
  is '参数备注';
comment on column SYS_PARAM.status
  is '当前状态(0:停用;1:启用)';
comment on column SYS_PARAM.edit_mode
  is '编辑模式(0:只读;1:可编辑)';
comment on column SYS_PARAM.create_time
  is '创建时间';
comment on column SYS_PARAM.create_user_id
  is '创建用户编号';
comment on column SYS_PARAM.modify_time
  is '修改时间';
comment on column SYS_PARAM.modify_user_id
  is '修改用户ID';
alter table SYS_PARAM
  add constraint PK_PARAM_ID primary key (PARAM_ID);

prompt Creating SYS_ROLE...
create table SYS_ROLE
(
  role_id        VARCHAR2(50) not null,
  role_name      VARCHAR2(100) not null,
  status         VARCHAR2(10) default 1,
  role_type      VARCHAR2(10),
  role_remark    VARCHAR2(400),
  edit_mode      VARCHAR2(10) default 1,
  create_user_id VARCHAR2(50),
  create_time    DATE,
  modify_user_id VARCHAR2(50),
  modify_time    DATE
)
;
comment on table SYS_ROLE
  is '角色管理';
comment on column SYS_ROLE.role_id
  is '角色编号';
comment on column SYS_ROLE.role_name
  is '角色名称';
comment on column SYS_ROLE.status
  is '当前状态 1启用 0禁用';
comment on column SYS_ROLE.role_type
  is '角色类型';
comment on column SYS_ROLE.role_remark
  is '备注';
comment on column SYS_ROLE.edit_mode
  is '编辑模式(0:只读;1:可编辑)';
comment on column SYS_ROLE.create_user_id
  is '创建用户编号';
comment on column SYS_ROLE.create_time
  is '创建时间';
comment on column SYS_ROLE.modify_user_id
  is '修改用户编号';
comment on column SYS_ROLE.modify_time
  is '修改时间';
alter table SYS_ROLE
  add constraint PK_ROLE_ID primary key (ROLE_ID);

prompt Creating SYS_ROLE_MENU...
create table SYS_ROLE_MENU
(
  role_id        VARCHAR2(50) not null,
  menu_id        VARCHAR2(50) not null,
  grant_type     VARCHAR2(10),
  create_user_id VARCHAR2(50),
  create_time    DATE
)
;
comment on table SYS_ROLE_MENU
  is '角色与菜单关联表';
comment on column SYS_ROLE_MENU.role_id
  is '角色编号';
comment on column SYS_ROLE_MENU.menu_id
  is '菜单编号';
comment on column SYS_ROLE_MENU.grant_type
  is '权限类型 1 经办权限 2管理权限';
comment on column SYS_ROLE_MENU.create_user_id
  is '创建用户ID';
comment on column SYS_ROLE_MENU.create_time
  is '创建时间';

prompt Creating SYS_ROLE_USER...
create table SYS_ROLE_USER
(
  role_id        VARCHAR2(50) not null,
  user_id        VARCHAR2(50) not null,
  create_user_id VARCHAR2(50),
  create_time    DATE
)
;
comment on table SYS_ROLE_USER
  is '角色与用户关联表';
comment on column SYS_ROLE_USER.role_id
  is '角色编号';
comment on column SYS_ROLE_USER.user_id
  is '用户编号';
comment on column SYS_ROLE_USER.create_user_id
  is '创建用户ID';
comment on column SYS_ROLE_USER.create_time
  is '创建时间';

prompt Creating SYS_USER...
create table SYS_USER
(
  user_id        VARCHAR2(50) not null,
  account        VARCHAR2(50),
  password       VARCHAR2(50),
  username       VARCHAR2(50),
  lock_num       NUMBER(10) default 5,
  error_num      NUMBER(10) default 0,
  sex            VARCHAR2(10) default 3,
  status         VARCHAR2(10) default 1,
  user_type      VARCHAR2(10),
  dept_id        VARCHAR2(50),
  mobile         VARCHAR2(50),
  qq             VARCHAR2(50),
  wechat         VARCHAR2(50),
  email          VARCHAR2(50),
  idno           VARCHAR2(50),
  style          VARCHAR2(10) default 1,
  address        VARCHAR2(200),
  remark         VARCHAR2(400),
  is_del         VARCHAR2(10) default 0,
  create_time    DATE,
  create_user_id VARCHAR2(50),
  modify_time    DATE,
  modify_user_id VARCHAR2(50)
)
;
comment on table SYS_USER
  is '用户管理';
comment on column SYS_USER.user_id
  is '用户编号';
comment on column SYS_USER.account
  is '用户登录帐号';
comment on column SYS_USER.password
  is '密码';
comment on column SYS_USER.username
  is '用户姓名';
comment on column SYS_USER.lock_num
  is '锁定次数 默认5次';
comment on column SYS_USER.error_num
  is '密码错误次数  如果等于锁定次数就自动锁定用户';
comment on column SYS_USER.sex
  is '性别  1:男2:女3:未知';
comment on column SYS_USER.status
  is '用户状态 1:正常2:停用 3:锁定';
comment on column SYS_USER.user_type
  is '用户类型';
comment on column SYS_USER.dept_id
  is '所属部门流水号';
comment on column SYS_USER.mobile
  is '联系电话';
comment on column SYS_USER.qq
  is 'QQ号码';
comment on column SYS_USER.wechat
  is '微信';
comment on column SYS_USER.email
  is '邮箱';
comment on column SYS_USER.idno
  is '身份证号码';
comment on column SYS_USER.style
  is '界面风格';
comment on column SYS_USER.address
  is '联系地址';
comment on column SYS_USER.remark
  is '备注';
comment on column SYS_USER.is_del
  is '是否已删除 0有效 1删除';
comment on column SYS_USER.create_time
  is '创建时间';
comment on column SYS_USER.create_user_id
  is '创建人ID';
comment on column SYS_USER.modify_time
  is '修改时间';
comment on column SYS_USER.modify_user_id
  is '修改用户编号';
alter table SYS_USER
  add constraint PK_USER_ID primary key (USER_ID);

prompt Loading IH_SCHOOL...
prompt Table is empty
prompt Loading IH_UNIT...
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('b840c0d7d33c448c8b60cf67b67f1986', '新华街计生办', '1', '2', '新华街建设路1号新华街政务中心', '86801862', to_date('07-03-2017 21:15:27', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('07-03-2017 21:15:27', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 1, '2');
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('71b48540478f44a9a0fd8c37deae49d9', '新雅街计生办', '2', '2', '新雅街雅源中路18号新雅街道办事处', '86839802', to_date('08-03-2017 16:28:42', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('08-03-2017 16:28:42', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 2, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('f5e51bfceff0467d9fe1514e407577d8', '花山镇计生办', '5', '2', '花山镇花山大道一号', '86848515', to_date('10-03-2017 18:06:20', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:06:20', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 5, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('4b8ee990136145b4bca751ad5c7f29d8', '花东镇计生办', '6', '2', '花东镇政务服务中心（南溪村委旁）', '86777681', to_date('10-03-2017 18:06:47', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:06:47', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 6, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('f1e48d98d8a6459b8b586ae12345e9fa', '狮岭镇计生办', '8', '2', '狮岭镇南航大道与历成中路交界处68号', '36919553', to_date('10-03-2017 18:07:14', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:07:14', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 7, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('8f2e1428af0646d89cc6815382f681fb', '炭步镇计生办', '9', '2', '炭步镇园华路2号炭步镇政务中心', '86733668', to_date('10-03-2017 18:07:36', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:07:36', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 8, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('9c391e8cbb25441294a57c8b090229a7', '赤坭镇计生办', '7', '2', '赤坭镇新兴路5号', '86841668', to_date('10-03-2017 18:07:59', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:07:59', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 9, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('e2a61d848a914086a91669242ffcfcc2', '梯面镇计生办', '10', '2', '梯面镇金梯大道23号', '86851598', to_date('10-03-2017 18:08:23', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:08:23', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 10, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('80517f5f181e4e36b0cd65120b98b761', '新华街流管中心', '1', '3', '建设北路105号', '36999360', to_date('10-03-2017 18:09:47', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:09:47', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 11, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('d7e502d758f54f0f9ff6de16d51bf357', '新雅街流管中心', '2', '3', '雅瑶中路自编18号', '86837123', to_date('10-03-2017 18:10:16', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:10:16', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 12, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('e3f9a4d4ead24b2982dc6bf5522894e9', '秀全街流管中心', '4', '3', '迎宾大道168号首层', '36861161', to_date('10-03-2017 18:10:37', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:10:37', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 13, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('f9ffb5c4af234f7b87621ddeeb27c1b9', '花城街流管中心', '3', '3', '建设北路191号', '89689379', to_date('10-03-2017 18:11:03', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:11:03', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 14, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('c8785677b2bb4fe98452f90321c689bc', '狮岭镇流管中心', '8', '3', '狮岭镇阳光南路15号', '86931383', to_date('10-03-2017 18:11:27', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:11:27', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 15, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('878c3b98c7444ae090d11e15c2640247', '花山镇社会事务中心', '5', '3', '花山镇106国道市政大学对面', '86781981', to_date('10-03-2017 18:11:56', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:11:56', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 16, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('63e833e14fe9402e8d89892720ec1a89', '花东镇流管中心', '6', '3', '花东镇东成路（花东派出所西侧）', '86761555', to_date('10-03-2017 18:12:18', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:12:18', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 17, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('682c75247f1842309200a2ce27c55699', '炭步镇流管中心', '9', '3', '炭步镇北街路26号', '86730388', to_date('10-03-2017 18:12:48', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:12:48', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 18, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('c17f9e2feb3a4f09aefcd0801356055d', '赤坭镇社会事务中心', '7', '3', '赤坭镇广源东路8号', '86718311', to_date('10-03-2017 18:13:09', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:13:09', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 19, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('80972ba323584b7cb3e6d8c80822e60b', '梯面镇社会事务中心', '10', '3', '梯面镇金梯大道22号', '86851598', to_date('10-03-2017 18:13:31', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:13:31', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 20, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('2670e4d23b374ccb8c56d1055f5fbd43', '区社保办', '0', '4', '花都区花城街公益大道兰花路1号', '86969113', to_date('10-03-2017 18:13:58', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:13:58', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 21, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('5e975d13abbb408faaf0754e32c4ccb2', '区医保办', '0', '5', '花都区花城街公益大道兰花路1号', '86969113', to_date('10-03-2017 18:14:19', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:14:19', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 22, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('7f559f50ffaf46fdabb887838cfc1494', '区劳动就业服务管理中心', '0', '6', '公园前路15号', '86811651', to_date('10-03-2017 18:14:42', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:14:42', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 23, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('c0b36033a6c542399166de59c74f20c4', '花城街计生办', '3', '2', '花城街紫薇路50号花城街政务中心', '36915221', to_date('10-03-2017 18:05:18', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:05:18', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 3, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('5e29828f06794dc195a76665b8650688', '秀全街计生办', '4', '2', '秀全街迎宾大道西168号秀全街政务中心', '36866993', to_date('10-03-2017 18:05:52', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:05:52', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 4, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('aec385db944d4d558150e881a17d23b4', '区人社局事业单位管理科', '0', '9', '兰花路1号人社局1号楼1307室', '36911262', to_date('10-03-2017 18:15:54', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:15:54', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 24, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('1428830544ac4417bef394b3e8b88cea', '区国土资源与规划局', '0', '7', '迎宾大道5号之一，新都大桥脚', '36839838', to_date('10-03-2017 18:17:14', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:17:14', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 25, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('329639424eef47e38135eed5bea08422', '区公安分局办证大厅', '0', '8', '建设北路185号', '86972260', to_date('10-03-2017 18:17:40', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:17:40', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 26, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('5fb60fcc2758451e9719c9bb208ac262', '区来穗局', '0', '10', '天贵路94号', '36812938', to_date('10-03-2017 18:18:07', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('10-03-2017 18:18:07', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 27, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('a6a61821c2a04041af56967878d2d012', '北片教育指导中心', '5,10', '1', '花山镇两龙南街8号北片教育指导中心小学教研室（会议室旁）', '86943655', to_date('16-03-2017 14:58:46', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('16-03-2017 14:58:46', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 26, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('d8b9c55a3bfb48d087755b1b0121d797', '花东镇中心小学', '6', '1', '花东镇花都大道推广路段南9号（花东医院对面）', '86849312 86768201 86760826', to_date('16-03-2017 14:59:16', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('16-03-2017 14:59:16', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 28, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('fcf414f249ff4a25b6c9f9987263d1ab', '狮峰初级中学', '8', '1', '广州市花都区狮岭镇康政东路23号', '86918407', to_date('16-03-2017 15:00:47', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('16-03-2017 15:00:47', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 31, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('982ec30664c94cc7bf017f8b1cef7ecb', '合成小学', '8', '1', '广州市花都区狮岭镇合成村', '86927800', to_date('16-03-2017 15:02:03', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('16-03-2017 15:02:03', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 34, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('ed0821d9c4ae466588af7c467666ac63', '城区教育指导中心', '1,2,3,4', '1', '花都区新华街云山路67号新华七小', '36836030 36839729', to_date('16-03-2017 14:57:54', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('16-03-2017 14:57:54', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 25, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('6f3ec58c61124044a11b2d61c7ca5dcb', '联合小学', '8', '1', '广州市花都区狮岭镇联合村新联路', '86913951', to_date('16-03-2017 15:02:32', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('16-03-2017 15:02:32', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 35, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('c2203eb5b8fc45b19a80a0901cbf2209', '振兴二小', '8', '1', '广州市花都区狮岭镇盘古路101号', '86988728', to_date('16-03-2017 15:03:07', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('16-03-2017 15:03:07', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 36, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('8d5a6cf882dd4d55a3ac7a663a817a65', '炭步镇教育指导中心', '9', '1', '炭步镇市场西1号', '86843457', to_date('16-03-2017 14:59:43', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('16-03-2017 14:59:43', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 29, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('0d08ab3b5fd24406a0f17ba5edd7ee46', '赤坭镇教育指导中心', '7', '1', '赤坭镇赤坭大道30号之10', '86718391', to_date('16-03-2017 15:00:09', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('16-03-2017 15:00:09', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 30, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('4a7f687930fa41e2be007ead8b0ac59e', '冯村初级中学', '8', '1', '广州市花都区狮岭镇冯村中学', '86845307', to_date('16-03-2017 15:01:11', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('16-03-2017 15:01:11', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 32, null);
insert into IH_UNIT (unit_id, unit_name, unit_serve, unit_type, unit_address, unit_phone, create_time, create_user_id, modify_time, modify_user_id, sort_no, remark)
values ('8d660458c54e43ed8e92843d41fd8e23', '芙蓉初级中学', '8', '1', '广州市花都区狮岭镇旗岭育才路6号之一', '86854316', to_date('16-03-2017 15:01:39', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('16-03-2017 15:01:39', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', 33, null);
commit;
prompt 38 records loaded
prompt Loading SMS_EMAIL_OUTBOX...
insert into SMS_EMAIL_OUTBOX (email_id, subject, content, email_type, create_user_id, create_time)
values ('ee891f20a0094d34b6f018fc336a366f', '大项目', '大项目', '1', 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:26:32', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 1 records loaded
prompt Loading SMS_EMAIL_RECORD...
insert into SMS_EMAIL_RECORD (record_id, email_id, receive_email, receive_name, is_send, send_time, status)
values ('6ab85f698b5e4fe1bc9ee96ed95e849f', 'ee891f20a0094d34b6f018fc336a366f', '240823329@qq.com', '测试', '1', to_date('03-05-2017 10:26:44', 'dd-mm-yyyy hh24:mi:ss'), '1');
commit;
prompt 1 records loaded
prompt Loading SYS_CATALOG...
insert into SYS_CATALOG (catalog_id, cascade_id, root_key, root_name, catalog_name, parent_id, sort_no, icon_name, is_auto_expand, create_time, create_user_id, modify_time, modify_user_id)
values ('0e6cca42523b4f95afb8d138dc533e61', '0.002', 'DIC_TYPE', '字典分类科目', '数据字典分类', '0', 1, 'book', '0', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_CATALOG (catalog_id, cascade_id, root_key, root_name, catalog_name, parent_id, sort_no, icon_name, is_auto_expand, create_time, create_user_id, modify_time, modify_user_id)
values ('4f39839093744dccaedc9d7dcdee4ab3', '0.001', 'PARAM_TYPE', '参数分类', '参数分类科目', '0', 1, 'book', '0', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_CATALOG (catalog_id, cascade_id, root_key, root_name, catalog_name, parent_id, sort_no, icon_name, is_auto_expand, create_time, create_user_id, modify_time, modify_user_id)
values ('5423b9ba9ac6472c80881827acafe9e9', '0.001.001', 'PARAM_TYPE', '参数分类', '系统参数', '4f39839093744dccaedc9d7dcdee4ab3', 1, 'folder', '1', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_CATALOG (catalog_id, cascade_id, root_key, root_name, catalog_name, parent_id, sort_no, icon_name, is_auto_expand, create_time, create_user_id, modify_time, modify_user_id)
values ('6a75051434b840a597aeb549e1e47ced', '0.002.001', 'DIC_TYPE', '字典分类科目', '系统管理', '0e6cca42523b4f95afb8d138dc533e61', 2, 'folder', '1', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_CATALOG (catalog_id, cascade_id, root_key, root_name, catalog_name, parent_id, sort_no, icon_name, is_auto_expand, create_time, create_user_id, modify_time, modify_user_id)
values ('b420860910f54c1d8901f099a9457f52', '0.002.002', 'DIC_TYPE', '字典分类科目', '全局通用', '0e6cca42523b4f95afb8d138dc533e61', 1, 'global', '1', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
commit;
prompt 5 records loaded
prompt Loading SYS_DEPT...
insert into SYS_DEPT (dept_id, cascade_id, dept_name, parent_id, dept_code, manager, phone, fax, address, is_auto_expand, icon_name, sort_no, remark, is_del, create_time, create_user_id, modify_time, modify_user_id)
values ('0', '0', '组织机构', '-1', null, null, null, null, null, '1', 'dept_config', 1, '顶级机构不能进行移动和删除操作，只能进行修改', '0', to_date('18-01-2017 15:20:31', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_DEPT (dept_id, cascade_id, dept_name, parent_id, dept_code, manager, phone, fax, address, is_auto_expand, icon_name, sort_no, remark, is_del, create_time, create_user_id, modify_time, modify_user_id)
values ('304184e5156d4a4abb7a7218ee84f2e2', '0.0001', '23232332', '0', '3是范德萨发', null, null, null, null, '1', null, 1, null, '1', to_date('19-01-2017 11:08:40', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('22-01-2017 13:52:48', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
commit;
prompt 2 records loaded
prompt Loading SYS_DICTIONARY...
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('ae623964bbb8431d8f62d2adf7784b28', '97ec40cfa09f4531ad1c8485885fe57b', '3', '业务邮件', null, '1', '1', 3, to_date('03-05-2017 10:28:58', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:29:06', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('e664bd31823a4384ac3f3ce5938094cd', 'a1a2e8d035934d978e44bbd965db743e', '4', '秀全街', null, '1', '1', 5, to_date('03-05-2017 10:56:33', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:57:27', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('116e56b0d56f46c8a955533bc7072fa8', 'a1a2e8d035934d978e44bbd965db743e', '5', '花生镇', null, '1', '1', 6, to_date('03-05-2017 10:57:04', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:57:21', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('ee53ff692f384230b7da89eeef28f2c1', 'a1a2e8d035934d978e44bbd965db743e', '6', '花东镇', null, '1', '1', 7, to_date('03-05-2017 10:57:45', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:57:45', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('7d56a647ebb14ff895c1dcffbe5539e9', 'a1a2e8d035934d978e44bbd965db743e', '7', '赤坭镇', null, '1', '1', 8, to_date('03-05-2017 10:58:05', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:58:05', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('d60b600511bd4f1eaeb72c1aab35d229', 'a1a2e8d035934d978e44bbd965db743e', '8', '狮岭镇', null, '1', '1', 9, to_date('03-05-2017 10:58:21', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:58:21', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('a6b6cce2517943508e1cacde7bb486a8', 'a1a2e8d035934d978e44bbd965db743e', '9', '炭步镇', null, '1', '1', 10, to_date('03-05-2017 10:59:54', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:59:54', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('ddf8fbadb1fc40f49fede49655985db5', 'a1a2e8d035934d978e44bbd965db743e', '10', '梯面镇', null, '1', '1', 11, to_date('03-05-2017 11:00:06', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:00:06', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('c15b3e8b29a0406c81a4318d7bfaba39', 'a1a2e8d035934d978e44bbd965db743e', '1', '新华街', null, '1', '1', 2, to_date('03-05-2017 10:55:38', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:55:38', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('08d76c1845ab425498810e716a8621e6', 'a9f98697527e452eaa4540e60ae98dc6', '0', '未发送', null, '1', '1', 1, to_date('03-05-2017 10:31:43', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:31:43', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('3010501d2d47432ab63c8ac40a9a5c0c', 'a1a2e8d035934d978e44bbd965db743e', '0', '全区', null, '1', '1', 1, to_date('03-05-2017 10:55:22', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:55:22', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('f4309055e8dd450489a85d37b2a4003f', 'a1a2e8d035934d978e44bbd965db743e', '2', '新雅街', null, '1', '1', 3, to_date('03-05-2017 10:55:48', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:55:57', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('b98e939f0c1a4a658bce8cbcf5c0dbaa', 'a1a2e8d035934d978e44bbd965db743e', '3', '花城街', null, '1', '1', 4, to_date('03-05-2017 10:56:22', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:56:22', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('e823709cdff74b1dac9d6a3204af515f', 'a1a2e8d035934d978e44bbd965db743e', '11', '其他区', null, '1', '1', 12, to_date('03-05-2017 11:00:22', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:00:22', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('8dfd45e6ccf94460b1b979c21d1b1806', '99fd0f3f2d1a49c1acd97ea22415e4a8', '1', '缺省', null, '1', '1', 1, to_date('03-05-2017 09:58:58', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 09:58:58', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('93e4051729ac4586979a52bd5617740f', '97ec40cfa09f4531ad1c8485885fe57b', '4', '定时邮件', null, '1', '1', 4, to_date('03-05-2017 10:29:26', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:29:26', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('78107194c65146249c31f82a0e565bea', '6d38696cb3f0457cb69831c3ef3b02b2', '0', '投递失败', null, '1', '1', 1, to_date('03-05-2017 10:30:11', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:30:11', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('b5d9c2328ce749a9913a7cf4def3b743', '6d38696cb3f0457cb69831c3ef3b02b2', '1', '投递成功', null, '1', '1', 1, to_date('03-05-2017 10:30:21', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:30:21', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('a5be19d6d85b424d9e719df8aa4cfdb5', 'be4c44e7d83a4f508caa3e893e4c3360', '1', '教育', null, '1', '1', 1, to_date('03-05-2017 11:06:08', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:06:08', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('5c26404f35284e7a9bdb373fa4518303', 'be4c44e7d83a4f508caa3e893e4c3360', '2', '计生', null, '1', '1', 2, to_date('03-05-2017 11:06:24', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:06:24', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('4f9d305c5b3f44c8851a6f4b3107ee8f', 'be4c44e7d83a4f508caa3e893e4c3360', '3', '住房保障-租房', null, '1', '1', 3, to_date('03-05-2017 11:06:43', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:06:43', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('9fc8ed31830c48a7a32f613bce856d3f', 'be4c44e7d83a4f508caa3e893e4c3360', '4', '社保', null, '1', '1', 4, to_date('03-05-2017 11:06:54', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:06:54', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('54b51e8f289d47adbcf9620f1fac05f8', 'be4c44e7d83a4f508caa3e893e4c3360', '9', '职称', null, '1', '1', 9, to_date('03-05-2017 11:07:58', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:07:58', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('3dbd1442f9c04ea0adfa164df1e09d87', 'be4c44e7d83a4f508caa3e893e4c3360', '10', '居住证审核', null, '1', '1', 10, to_date('03-05-2017 11:08:10', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:08:10', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('14e6a623eeb14fd786e3c51e667b89e5', 'be4c44e7d83a4f508caa3e893e4c3360', '11', '就业服务中心-个体户', null, '1', '1', 11, to_date('03-05-2017 11:08:23', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:08:23', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('1c3537c22d2b4f72a59d4fcc14940c1c', 'a9f98697527e452eaa4540e60ae98dc6', '1', '已发送', null, '1', '1', 2, to_date('03-05-2017 10:31:57', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:31:57', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('c6ccaf5afb0d46dcbe1ed50815047291', 'be4c44e7d83a4f508caa3e893e4c3360', '8', '公安局', null, '1', '1', 8, to_date('03-05-2017 11:07:47', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:07:47', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('54caa61a5d354c678bb7858023ff3e6c', '97ec40cfa09f4531ad1c8485885fe57b', '2', '验证邮件', null, '1', '1', 2, to_date('03-05-2017 10:28:44', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:28:44', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('5df40b4768ff4233bf17d09406532b9d', '032b31e5b64e40f6b3140e6b96dcbc1c', '1', '小学', null, '1', '1', 1, to_date('03-05-2017 11:01:17', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:03:47', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('11b823f3b2e14e76bf94347a4a5e578e', 'c48507ef391d4e3d8d9b7720efe4841b', '0', '停用', null, '1', '0', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('293adbde400f457a8d947ff5c6341b04', '992a7d6dbe7f4009b30cbae97c3b64a9', '3', '锁定', '#FFA500', '1', '1', 3, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('2ac97527c4924127b742dd953d8b53ba', '820d2a68425b4d8d9b423b81d6a0eec1', '3', '未知', null, '1', '1', 3, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('2bfc90a6917545cd87d73fb491292e2b', 'aaec0092a25b485f90c20898e9d6765d', '1', '缺省', null, '1', '1', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('3cf6af08f48e4cec913d09f67a0b3b43', '992a7d6dbe7f4009b30cbae97c3b64a9', '1', '正常', null, '1', '1', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('913ca1b4b49a434fb9591f6df0a52af8', 'c6f8b99b95c844b89dc86c143e04a294', '0', '否', null, '1', '0', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('9c63657b98c444e3bfd8a0a75128de2b', '7a7faf68a5ec4f3cb9f45d89c119b26b', '0', '只读', null, '1', '0', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('a96dfb72b7b54e1989569a2b3c5f90ac', 'c48507ef391d4e3d8d9b7720efe4841b', '1', '启用', null, '1', '0', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('ca40ef37acef49f8930fcf22356ba50e', 'c6f8b99b95c844b89dc86c143e04a294', '1', '是', null, '1', '0', 2, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('d2cf230ce49040e3bf6e61a972659c09', '992a7d6dbe7f4009b30cbae97c3b64a9', '2', '停用', 'red', '1', '1', 2, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('d404e540aab945df84a26e3d30b2dd47', '820d2a68425b4d8d9b423b81d6a0eec1', '2', '女', null, '1', '1', 2, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('d7f0c4a5480d4dc4b3e6e4c5b405d9cb', '820d2a68425b4d8d9b423b81d6a0eec1', '1', '男', null, '1', '1', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('f1c0ae8844504f96836b904ce81ac1bc', '7a7faf68a5ec4f3cb9f45d89c119b26b', '1', '可编辑', null, '1', '0', 2, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('e0e59a52f42c4263aac3e9dbbdb496df', '0bf2a3cd7ed44516a261347d47995411', '1', '经典风格', null, '1', '1', 1, to_date('06-02-2017 14:27:56', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('06-02-2017 14:27:56', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('82afb0bda8944af3a0e5f82608294670', '0bf2a3cd7ed44516a261347d47995411', '2', '顶部布局', null, '1', '1', 2, to_date('06-02-2017 14:29:28', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('06-02-2017 14:55:14', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('d8642fc74f5e4824b1254705285f1264', '97ec40cfa09f4531ad1c8485885fe57b', '1', '普通邮件', null, '1', '1', 1, to_date('03-05-2017 10:28:28', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:28:28', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('8f267471c92d4fe9b17c977abbea0f4e', '032b31e5b64e40f6b3140e6b96dcbc1c', '2', '中学', null, '1', '1', 2, to_date('03-05-2017 11:01:30', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:03:55', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('e5adcaa5d9cb4612abd14bd935080b50', 'be4c44e7d83a4f508caa3e893e4c3360', '5', '医保', null, '1', '1', 5, to_date('03-05-2017 11:07:09', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:07:09', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('0a4de242d4cf4945818fdaf15d09ae7a', 'be4c44e7d83a4f508caa3e893e4c3360', '6', '就业服务中心-员工', null, '1', '1', 6, to_date('03-05-2017 11:07:21', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:07:21', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY (dic_id, dic_index_id, dic_code, dic_value, show_color, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id)
values ('5e311168372a433288e49a259334e8d7', 'be4c44e7d83a4f508caa3e893e4c3360', '7', '住房保障- 产权房', null, '1', '1', 7, to_date('03-05-2017 11:07:32', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:07:32', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
commit;
prompt 49 records loaded
prompt Loading SYS_DICTIONARY_INDEX...
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('032b31e5b64e40f6b3140e6b96dcbc1c', 'school_type', '学位类型', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('03-05-2017 11:00:50', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:03:40', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('a9f98697527e452eaa4540e60ae98dc6', 'is_send', '是否发送', '0e6cca42523b4f95afb8d138dc533e61', '0.002', null, to_date('03-05-2017 10:31:28', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:31:28', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('a1a2e8d035934d978e44bbd965db743e', 'street_type', '街镇', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('03-05-2017 10:54:59', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:54:59', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('7a7faf68a5ec4f3cb9f45d89c119b26b', 'edit_mode', '编辑模式', 'b420860910f54c1d8901f099a9457f52', '0.002.002', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('820d2a68425b4d8d9b423b81d6a0eec1', 'sex', '性别', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('992a7d6dbe7f4009b30cbae97c3b64a9', 'user_status', '用户状态', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('aaec0092a25b485f90c20898e9d6765d', 'role_type', '角色类型', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('c48507ef391d4e3d8d9b7720efe4841b', 'status', '当前状态', 'b420860910f54c1d8901f099a9457f52', '0.002.002', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('c6f8b99b95c844b89dc86c143e04a294', 'is_auto_expand', '是否自动展开', 'b420860910f54c1d8901f099a9457f52', '0.002.002', null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('99fd0f3f2d1a49c1acd97ea22415e4a8', 'menu_type', '菜单类型', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('03-05-2017 09:58:49', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 09:58:49', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('6d38696cb3f0457cb69831c3ef3b02b2', 'email_status', '投递状态', '0e6cca42523b4f95afb8d138dc533e61', '0.002', null, to_date('03-05-2017 10:29:49', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:29:49', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('be4c44e7d83a4f508caa3e893e4c3360', 'unit_type', '职能单位类型', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('03-05-2017 11:05:53', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 11:05:53', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('0bf2a3cd7ed44516a261347d47995411', 'layout_style', '界面布局', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, to_date('06-02-2017 14:27:28', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('06-02-2017 14:27:28', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_DICTIONARY_INDEX (dic_index_id, dic_key, dic_name, catalog_id, catalog_cascade_id, dic_remark, create_time, create_user_id, modify_time, modify_user_id)
values ('97ec40cfa09f4531ad1c8485885fe57b', 'email_type', '邮件类型', '0e6cca42523b4f95afb8d138dc533e61', '0.002', null, to_date('03-05-2017 10:28:09', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:28:09', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
commit;
prompt 14 records loaded
prompt Loading SYS_MENU...
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('7f4f6a3b2a0d48f998414d01a00fcbb3', '0.002.001', '邮件管理', 'de799fc73ad841d0af51bf7847e3c21d', 'email', '1', 'system/email/initEmailRecord.jhtml', null, '1', '1', 1, to_date('03-05-2017 10:16:39', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:16:53', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', '1');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('de799fc73ad841d0af51bf7847e3c21d', '0.002', '信息管理', '0', 'msg_config', '1', null, null, '1', '1', 2, to_date('03-05-2017 10:14:14', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:14:14', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', '1');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('1ae9eeb251a243abb4c0c4f3865d6262', '0.001.001', '菜单配置', 'c66886c9ee47415aa81a6589acdb480a', 'menu_config', '1', 'system/menu/init.jhtml', null, '1', '1', 4, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null, '1');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('8eefe6e3298c4203b21e85e354b284ab', '0.001.004', '分类科目', 'c66886c9ee47415aa81a6589acdb480a', 'catalog', '1', 'system/catalog/init.jhtml', null, '1', '1', 7, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null, '1');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('94ca69dd59b84054ad056b130d959425', '0.001.003', '键值参数', 'c66886c9ee47415aa81a6589acdb480a', 'param_config', '1', 'system/param/init.jhtml', null, '1', '1', 6, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, null, null, '1');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('a5b39c90931b4249a23e30f24303dbfa', '0.001.002', '数据字典', 'c66886c9ee47415aa81a6589acdb480a', 'dictionary', '1', 'system/dictionary/init.jhtml', null, '1', '0', 5, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, '1');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('c66886c9ee47415aa81a6589acdb480a', '0.001', '系统管理', '0', 'system_manage', '1', null, null, '1', '1', 10, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('23-01-2017 17:19:34', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', '1');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('d525b1f3274244c9af3a06c4e72621d8', '0.001.009', '角色管理', 'c66886c9ee47415aa81a6589acdb480a', 'group_link', '1', 'system/role/init.jhtml', null, '1', '1', 3, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, '1');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('dfc4df3c7f4341f885caf7aa305f8995', '0.001.007', ' 组织机构', 'c66886c9ee47415aa81a6589acdb480a', 'dept_config', '1', 'system/dept/init.jhtml', null, '1', '1', 1, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, '1');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('f278c724ce8e4649bc2b74333ac0d28c', '0.001.008', '用户管理', 'c66886c9ee47415aa81a6589acdb480a', 'user_config', '1', 'system/user/init.jhtml', null, '1', '1', 2, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, '1');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('0a6d1c03198a451da47bb3442145bc34', '0.002.003', '学校管理', 'de799fc73ad841d0af51bf7847e3c21d', 'school', '1', 'business/school/init.jhtml', null, '1', '1', 3, to_date('03-05-2017 10:53:02', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:53:02', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', '1');
insert into SYS_MENU (menu_id, cascade_id, menu_name, parent_id, icon_name, is_auto_expand, url, remark, status, edit_mode, sort_no, create_time, create_user_id, modify_time, modify_user_id, menu_type)
values ('d806eed4d2ce411f807422dbd1df35e5', '0.002.002', '单位管理', 'de799fc73ad841d0af51bf7847e3c21d', 'unit_config', '1', 'business/unit/init.jhtml', null, '1', '1', 2, to_date('03-05-2017 10:51:30', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:51:30', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', '1');
commit;
prompt 12 records loaded
prompt Loading SYS_PARAM...
insert into SYS_PARAM (param_id, param_name, param_key, param_value, catalog_id, catalog_cascade_id, param_remark, status, edit_mode, create_time, create_user_id, modify_time, modify_user_id)
values ('c4df22d4d63d4d5ea136eff79b65dc69', '邮件开关', 'email_switch', 'on', '4f39839093744dccaedc9d7dcdee4ab3', '0.001', '邮件开关 on:开 off:关', '1', '1', to_date('03-05-2017 10:22:50', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('03-05-2017 10:25:40', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
commit;
prompt 1 records loaded
prompt Loading SYS_ROLE...
prompt Table is empty
prompt Loading SYS_ROLE_MENU...
prompt Table is empty
prompt Loading SYS_ROLE_USER...
prompt Table is empty
prompt Loading SYS_USER...
insert into SYS_USER (user_id, account, password, username, lock_num, error_num, sex, status, user_type, dept_id, mobile, qq, wechat, email, idno, style, address, remark, is_del, create_time, create_user_id, modify_time, modify_user_id)
values ('31ee508880ef4b27bea9d95d3156fc8a', 't1', 'FyzQw6C0Ct4=', '112', 5, 0, '3', '1', '1', '0', null, null, null, null, null, null, null, null, '1', to_date('18-01-2017 12:31:54', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('18-01-2017 12:35:10', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_USER (user_id, account, password, username, lock_num, error_num, sex, status, user_type, dept_id, mobile, qq, wechat, email, idno, style, address, remark, is_del, create_time, create_user_id, modify_time, modify_user_id)
values ('9073ca4cf33e49cbba7c1d44db9a2bc4', 't2', 'FyzQw6C0Ct4=', '1', 5, 0, '3', '1', '1', '0', null, null, null, null, null, null, null, null, '1', to_date('18-01-2017 12:32:11', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317', to_date('18-01-2017 12:32:16', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
insert into SYS_USER (user_id, account, password, username, lock_num, error_num, sex, status, user_type, dept_id, mobile, qq, wechat, email, idno, style, address, remark, is_del, create_time, create_user_id, modify_time, modify_user_id)
values ('cb33c25f5c664058a111a9b876152317', 'super', '0d+ywCe6ffI=', '超级用户', 10, 0, '2', '1', '2', '0', '13802907704', '240823329', null, '240823329@qq.com', null, '1', null, '超级用户，拥有最高的权限', '0', to_date('07-10-2016 23:49:14', 'dd-mm-yyyy hh24:mi:ss'), null, to_date('03-05-2017 11:10:20', 'dd-mm-yyyy hh24:mi:ss'), 'cb33c25f5c664058a111a9b876152317');
commit;
prompt 3 records loaded
set feedback on
set define on
prompt Done.
