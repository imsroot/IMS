/*
Navicat MySQL Data Transfer

Source Server         : mycon
Source Server Version : 50162
Source Host           : localhost:3306
Source Database       : ims

Target Server Type    : MYSQL
Target Server Version : 50162
File Encoding         : 65001

Date: 2017-07-07 22:07:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ih_school`
-- ----------------------------
DROP TABLE IF EXISTS `ih_school`;
CREATE TABLE `ih_school` (
  `school_id` varchar(50) NOT NULL,
  `street` varchar(10) DEFAULT NULL COMMENT '所属街镇',
  `school_name` varchar(100) DEFAULT NULL COMMENT '学校名称',
  `places` int(10) DEFAULT NULL COMMENT '名额',
  `school_address` varchar(200) DEFAULT NULL COMMENT '学校地址',
  `school_type` varchar(10) DEFAULT NULL COMMENT '学校类型',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`school_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学校信息表';

-- ----------------------------
-- Records of ih_school
-- ----------------------------

-- ----------------------------
-- Table structure for `ih_unit`
-- ----------------------------
DROP TABLE IF EXISTS `ih_unit`;
CREATE TABLE `ih_unit` (
  `unit_id` varchar(50) NOT NULL,
  `unit_name` varchar(100) DEFAULT NULL,
  `unit_serve` varchar(100) DEFAULT NULL,
  `unit_type` varchar(10) DEFAULT NULL,
  `unit_address` varchar(200) DEFAULT NULL,
  `unit_phone` varchar(100) DEFAULT NULL,
  `sort_no` int(10) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户ID',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单位信息表';

-- ----------------------------
-- Records of ih_unit
-- ----------------------------
INSERT INTO `ih_unit` VALUES ('0d08ab3b5fd24406a0f17ba5edd7ee46', '赤坭镇教育指导中心', '7', '1', '赤坭镇赤坭大道30号之10', '86718391', '30', null, '2017-05-03 12:27:20', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:20', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('1428830544ac4417bef394b3e8b88cea', '区国土资源与规划局', '0', '7', '迎宾大道5号之一，新都大桥脚', '36839838', '25', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('2670e4d23b374ccb8c56d1055f5fbd43', '区社保办', '0', '4', '花都区花城街公益大道兰花路1号', '86969113', '21', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('329639424eef47e38135eed5bea08422', '区公安分局办证大厅', '0', '8', '建设北路185号', '86972260', '26', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('4a7f687930fa41e2be007ead8b0ac59e', '冯村初级中学', '8', '1', '广州市花都区狮岭镇冯村中学', '86845307', '32', null, '2017-05-03 12:28:22', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:28:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('4b8ee990136145b4bca751ad5c7f29d8', '花东镇计生办', '6', '2', '花东镇政务服务中心（南溪村委旁）', '86777681', '6', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('5e29828f06794dc195a76665b8650688', '秀全街计生办', '4', '2', '秀全街迎宾大道西168号秀全街政务中心', '36866993', '4', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('5e975d13abbb408faaf0754e32c4ccb2', '区医保办', '0', '5', '花都区花城街公益大道兰花路1号', '86969113', '22', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('5fb60fcc2758451e9719c9bb208ac262', '区来穗局', '0', '10', '天贵路94号', '36812938', '27', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('63e833e14fe9402e8d89892720ec1a89', '花东镇流管中心', '6', '3', '花东镇东成路（花东派出所西侧）', '86761555', '17', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('682c75247f1842309200a2ce27c55699', '炭步镇流管中心', '9', '3', '炭步镇北街路26号', '86730388', '18', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('6f3ec58c61124044a11b2d61c7ca5dcb', '联合小学', '8', '1', '广州市花都区狮岭镇联合村新联路', '86913951', '35', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('71b48540478f44a9a0fd8c37deae49d9', '新雅街计生办', '2', '2', '新雅街雅源中路18号新雅街道办事处', '86839802', '2', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('7f559f50ffaf46fdabb887838cfc1494', '区劳动就业服务管理中心', '0', '6', '公园前路15号', '86811651', '23', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('80517f5f181e4e36b0cd65120b98b761', '新华街流管中心', '1', '3', '建设北路105号', '36999360', '11', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('80972ba323584b7cb3e6d8c80822e60b', '梯面镇社会事务中心', '10', '3', '梯面镇金梯大道22号', '86851598', '20', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('878c3b98c7444ae090d11e15c2640247', '花山镇社会事务中心', '5', '3', '花山镇106国道市政大学对面', '86781981', '16', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('8d5a6cf882dd4d55a3ac7a663a817a65', '炭步镇教育指导中心', '9', '1', '炭步镇市场西1号', '86843457', '29', null, '2017-05-03 12:27:20', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:20', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('8d660458c54e43ed8e92843d41fd8e23', '芙蓉初级中学', '8', '1', '广州市花都区狮岭镇旗岭育才路6号之一', '86854316', '33', null, '2017-05-03 12:28:22', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:28:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('8f2e1428af0646d89cc6815382f681fb', '炭步镇计生办', '9', '2', '炭步镇园华路2号炭步镇政务中心', '86733668', '8', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('982ec30664c94cc7bf017f8b1cef7ecb', '合成小学', '8', '1', '广州市花都区狮岭镇合成村', '86927800', '34', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('9c391e8cbb25441294a57c8b090229a7', '赤坭镇计生办', '7', '2', '赤坭镇新兴路5号', '86841668', '9', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('a6a61821c2a04041af56967878d2d012', '北片教育指导中心', '5,10', '1', '花山镇两龙南街8号北片教育指导中心小学教研室（会议室旁）', '86943655', '26', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('aec385db944d4d558150e881a17d23b4', '区人社局事业单位管理科', '0', '9', '兰花路1号人社局1号楼1307室', '36911262', '24', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('b840c0d7d33c448c8b60cf67b67f1986', '新华街计生办', '1', '2', '新华街建设路1号新华街政务中心', '86801862', '1', '2', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('c0b36033a6c542399166de59c74f20c4', '花城街计生办', '3', '2', '花城街紫薇路50号花城街政务中心', '36915221', '3', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('c17f9e2feb3a4f09aefcd0801356055d', '赤坭镇社会事务中心', '7', '3', '赤坭镇广源东路8号', '86718311', '19', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('c2203eb5b8fc45b19a80a0901cbf2209', '振兴二小', '8', '1', '广州市花都区狮岭镇盘古路101号', '86988728', '36', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('c8785677b2bb4fe98452f90321c689bc', '狮岭镇流管中心', '8', '3', '狮岭镇阳光南路15号', '86931383', '15', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('d7e502d758f54f0f9ff6de16d51bf357', '新雅街流管中心', '2', '3', '雅瑶中路自编18号', '86837123', '12', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('d8b9c55a3bfb48d087755b1b0121d797', '花东镇中心小学', '6', '1', '花东镇花都大道推广路段南9号（花东医院对面）', '86849312 86768201 86760826', '28', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('e2a61d848a914086a91669242ffcfcc2', '梯面镇计生办', '10', '2', '梯面镇金梯大道23号', '86851598', '10', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('e3f9a4d4ead24b2982dc6bf5522894e9', '秀全街流管中心', '4', '3', '迎宾大道168号首层', '36861161', '13', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('ed0821d9c4ae466588af7c467666ac63', '城区教育指导中心', '1,2,3,4', '1', '花都区新华街云山路67号新华七小', '36836030 36839729', '25', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('f1e48d98d8a6459b8b586ae12345e9fa', '狮岭镇计生办', '8', '2', '狮岭镇南航大道与历成中路交界处68号', '36919553', '7', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('f5e51bfceff0467d9fe1514e407577d8', '花山镇计生办', '5', '2', '花山镇花山大道一号', '86848515', '5', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('f9ffb5c4af234f7b87621ddeeb27c1b9', '花城街流管中心', '3', '3', '建设北路191号', '89689379', '14', null, '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `ih_unit` VALUES ('fcf414f249ff4a25b6c9f9987263d1ab', '狮峰初级中学', '8', '1', '广州市花都区狮岭镇康政东路23号', '86918407', '31', null, '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:27:19', 'cb33c25f5c664058a111a9b876152317');

-- ----------------------------
-- Table structure for `sms_email_outbox`
-- ----------------------------
DROP TABLE IF EXISTS `sms_email_outbox`;
CREATE TABLE `sms_email_outbox` (
  `email_id` varchar(50) NOT NULL COMMENT '邮件类型',
  `subject` varchar(100) DEFAULT NULL COMMENT '邮件主题',
  `content` varchar(4000) DEFAULT NULL COMMENT '邮件内容',
  `email_type` varchar(10) DEFAULT '1' COMMENT '邮件类型 1 普通邮件 2 验证邮件 3业务邮件 4定时邮件',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮件发件箱';

-- ----------------------------
-- Records of sms_email_outbox
-- ----------------------------
INSERT INTO `sms_email_outbox` VALUES ('77dc1dbfc8984abf98156639de407e3c', '论文', '论文主要内容', '1', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 13:43:24');

-- ----------------------------
-- Table structure for `sms_email_record`
-- ----------------------------
DROP TABLE IF EXISTS `sms_email_record`;
CREATE TABLE `sms_email_record` (
  `record_id` varchar(50) NOT NULL COMMENT '邮件记录编号',
  `email_id` varchar(50) DEFAULT NULL COMMENT '发件箱编号',
  `receive_email` varchar(50) DEFAULT NULL COMMENT '接收邮箱',
  `receive_name` varchar(50) DEFAULT NULL COMMENT '接收人',
  `is_send` varchar(10) DEFAULT '0' COMMENT '是否发送 0 否 1是',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `status` varchar(10) DEFAULT '0' COMMENT '状态 0 失败 1 成功',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮件发件记录';

-- ----------------------------
-- Records of sms_email_record
-- ----------------------------
INSERT INTO `sms_email_record` VALUES ('c9613a620ee149a39e1ea2de9232f99f', '77dc1dbfc8984abf98156639de407e3c', '240823329@qq.com', '240823329', '1', '2017-05-03 13:47:21', '1');

-- ----------------------------
-- Table structure for `sys_catalog`
-- ----------------------------
DROP TABLE IF EXISTS `sys_catalog`;
CREATE TABLE `sys_catalog` (
  `catalog_id` varchar(50) NOT NULL DEFAULT '' COMMENT '分类科目编号',
  `cascade_id` varchar(500) DEFAULT NULL COMMENT '分类科目语义ID',
  `root_key` varchar(100) DEFAULT NULL COMMENT '科目标识键',
  `root_name` varchar(100) DEFAULT NULL COMMENT '科目名称',
  `catalog_name` varchar(50) DEFAULT NULL COMMENT '分类名称',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父节点编号',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序号',
  `icon_name` varchar(50) DEFAULT NULL COMMENT '图标名称',
  `is_auto_expand` varchar(10) DEFAULT '0' COMMENT '是否自动展开',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户ID',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`catalog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类科目';

-- ----------------------------
-- Records of sys_catalog
-- ----------------------------
INSERT INTO `sys_catalog` VALUES ('0e6cca42523b4f95afb8d138dc533e61', '0.002', 'DIC_TYPE', '字典分类科目', '数据字典分类', '0', '1', 'book', '0', '2016-10-07 23:49:14', null, '2016-10-07 23:49:31', null);
INSERT INTO `sys_catalog` VALUES ('4f39839093744dccaedc9d7dcdee4ab3', '0.001', 'PARAM_TYPE', '参数分类', '参数分类科目', '0', '1', 'book', '0', '2016-10-07 23:46:41', null, '2016-10-07 23:47:57', null);
INSERT INTO `sys_catalog` VALUES ('5423b9ba9ac6472c80881827acafe9e9', '0.001.001', 'PARAM_TYPE', '参数分类', '系统参数', '4f39839093744dccaedc9d7dcdee4ab3', '1', 'folder', '1', '2016-10-07 23:47:50', null, null, null);
INSERT INTO `sys_catalog` VALUES ('6a75051434b840a597aeb549e1e47ced', '0.002.001', 'DIC_TYPE', '字典分类科目', '系统管理', '0e6cca42523b4f95afb8d138dc533e61', '2', 'folder', '1', '2016-10-07 23:50:13', null, '2016-10-07 23:57:03', null);
INSERT INTO `sys_catalog` VALUES ('b420860910f54c1d8901f099a9457f52', '0.002.002', 'DIC_TYPE', '字典分类科目', '全局通用', '0e6cca42523b4f95afb8d138dc533e61', '1', 'global', '1', '2016-10-08 00:01:11', null, null, null);

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` varchar(50) NOT NULL COMMENT '流水号',
  `cascade_id` varchar(255) NOT NULL COMMENT '节点语义ID',
  `dept_name` varchar(100) NOT NULL COMMENT '组织名称',
  `parent_id` varchar(50) NOT NULL COMMENT '父节点流水号',
  `dept_code` varchar(50) DEFAULT NULL COMMENT '机构代码',
  `manager` varchar(50) DEFAULT NULL COMMENT '主要负责人',
  `phone` varchar(50) DEFAULT NULL COMMENT '部门电话',
  `fax` varchar(50) DEFAULT NULL COMMENT '传真',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `is_auto_expand` varchar(10) DEFAULT NULL COMMENT '是否自动展开',
  `icon_name` varchar(50) DEFAULT NULL COMMENT '节点图标文件名称',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序号',
  `remark` varchar(400) DEFAULT NULL COMMENT '备注',
  `is_del` varchar(10) DEFAULT '0' COMMENT '是否已删除 0有效 1删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建人ID',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='组织机构';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('0', '0', '组织机构', '-1', null, null, null, null, null, '1', 'dept_config', '1', '顶级机构不能进行移动和删除操作，只能进行修改', '0', '2016-12-29 10:13:20', null, null, null);
INSERT INTO `sys_dept` VALUES ('02448fead1be410ab4f0f7bea7285448', '0.0001', '广州研发中心', '0', null, null, null, null, null, '1', null, '1', null, '0', '2017-01-16 14:03:01', 'cb33c25f5c664058a111a9b876152317', '2017-01-16 14:03:01', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dept` VALUES ('728f6b0a20f0492dbd97b871620c2b19', '0.0001.0001', '天河区办事处', '02448fead1be410ab4f0f7bea7285448', null, null, null, null, null, '1', null, '1', null, '0', '2017-01-16 14:03:16', 'cb33c25f5c664058a111a9b876152317', '2017-01-16 14:03:16', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dept` VALUES ('fdc7155b279b47f58439c514fade3d8a', '0.0001.0002', '白云区办事处', '02448fead1be410ab4f0f7bea7285448', '', '', '', '', '', '1', '', '2', '', '1', '2017-01-16 14:03:32', 'cb33c25f5c664058a111a9b876152317', '2017-01-16 14:53:27', 'cb33c25f5c664058a111a9b876152317');

-- ----------------------------
-- Table structure for `sys_dictionary`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary`;
CREATE TABLE `sys_dictionary` (
  `dic_id` varchar(50) NOT NULL COMMENT '字典编号',
  `dic_index_id` varchar(255) DEFAULT NULL COMMENT '所属字典流水号',
  `dic_code` varchar(100) DEFAULT NULL COMMENT '字典对照码',
  `dic_value` varchar(100) DEFAULT NULL COMMENT '字典对照值',
  `show_color` varchar(50) DEFAULT NULL COMMENT '显示颜色',
  `status` varchar(10) DEFAULT '1' COMMENT '当前状态(0:停用;1:启用)',
  `edit_mode` varchar(10) DEFAULT '1' COMMENT '编辑模式(0:只读;1:可编辑)',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`dic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典';

-- ----------------------------
-- Records of sys_dictionary
-- ----------------------------
INSERT INTO `sys_dictionary` VALUES ('08d76c1845ab425498810e716a8621e6', 'a9f98697527e452eaa4540e60ae98dc6', '0', '未发送', null, '1', '1', '1', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('0a4de242d4cf4945818fdaf15d09ae7a', 'be4c44e7d83a4f508caa3e893e4c3360', '6', '就业服务中心-员工', null, '1', '1', '6', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('116e56b0d56f46c8a955533bc7072fa8', 'a1a2e8d035934d978e44bbd965db743e', '5', '花生镇', null, '1', '1', '6', '2017-05-03 12:44:20', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:20', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('11b823f3b2e14e76bf94347a4a5e578e', 'c48507ef391d4e3d8d9b7720efe4841b', '0', '停用', null, '1', '0', '1', '2017-05-03 12:44:22', null, '2017-05-03 12:44:22', null);
INSERT INTO `sys_dictionary` VALUES ('14e6a623eeb14fd786e3c51e667b89e5', 'be4c44e7d83a4f508caa3e893e4c3360', '11', '就业服务中心-个体户', null, '1', '1', '11', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('1c3537c22d2b4f72a59d4fcc14940c1c', 'a9f98697527e452eaa4540e60ae98dc6', '1', '已发送', null, '1', '1', '2', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('293adbde400f457a8d947ff5c6341b04', '992a7d6dbe7f4009b30cbae97c3b64a9', '3', '锁定', '#FFA500', '1', '1', '3', '2017-05-03 12:44:22', null, '2017-05-03 12:44:22', null);
INSERT INTO `sys_dictionary` VALUES ('2ac97527c4924127b742dd953d8b53ba', '820d2a68425b4d8d9b423b81d6a0eec1', '3', '未知', null, '1', '1', '3', '2017-05-03 12:44:22', null, '2017-05-03 12:44:22', null);
INSERT INTO `sys_dictionary` VALUES ('2bfc90a6917545cd87d73fb491292e2b', 'aaec0092a25b485f90c20898e9d6765d', '1', '缺省', null, '1', '1', '1', '2017-05-03 12:44:22', null, '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('3010501d2d47432ab63c8ac40a9a5c0c', 'a1a2e8d035934d978e44bbd965db743e', '0', '全区', null, '1', '1', '1', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('3cf6af08f48e4cec913d09f67a0b3b43', '992a7d6dbe7f4009b30cbae97c3b64a9', '1', '正常', null, '1', '1', '1', '2017-05-03 12:44:22', null, '2017-05-03 12:44:22', null);
INSERT INTO `sys_dictionary` VALUES ('3dbd1442f9c04ea0adfa164df1e09d87', 'be4c44e7d83a4f508caa3e893e4c3360', '10', '居住证审核', null, '1', '1', '10', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('4f9d305c5b3f44c8851a6f4b3107ee8f', 'be4c44e7d83a4f508caa3e893e4c3360', '3', '住房保障-租房', null, '1', '1', '3', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('54b51e8f289d47adbcf9620f1fac05f8', 'be4c44e7d83a4f508caa3e893e4c3360', '9', '职称', null, '1', '1', '9', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('54caa61a5d354c678bb7858023ff3e6c', '97ec40cfa09f4531ad1c8485885fe57b', '2', '验证邮件', null, '1', '1', '2', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('5c26404f35284e7a9bdb373fa4518303', 'be4c44e7d83a4f508caa3e893e4c3360', '2', '计生', null, '1', '1', '2', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('5df40b4768ff4233bf17d09406532b9d', '032b31e5b64e40f6b3140e6b96dcbc1c', '1', '小学', null, '1', '1', '1', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('5e311168372a433288e49a259334e8d7', 'be4c44e7d83a4f508caa3e893e4c3360', '7', '住房保障- 产权房', null, '1', '1', '7', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('78107194c65146249c31f82a0e565bea', '6d38696cb3f0457cb69831c3ef3b02b2', '0', '投递失败', null, '1', '1', '1', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('7d56a647ebb14ff895c1dcffbe5539e9', 'a1a2e8d035934d978e44bbd965db743e', '7', '赤坭镇', null, '1', '1', '8', '2017-05-03 12:44:20', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:20', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('82afb0bda8944af3a0e5f82608294670', '0bf2a3cd7ed44516a261347d47995411', '2', '顶部布局', null, '1', '1', '2', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('8dfd45e6ccf94460b1b979c21d1b1806', '99fd0f3f2d1a49c1acd97ea22415e4a8', '1', '缺省', null, '1', '1', '1', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('8f267471c92d4fe9b17c977abbea0f4e', '032b31e5b64e40f6b3140e6b96dcbc1c', '2', '中学', null, '1', '1', '2', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('913ca1b4b49a434fb9591f6df0a52af8', 'c6f8b99b95c844b89dc86c143e04a294', '0', '否', null, '1', '0', '1', '2017-05-03 12:44:23', null, '2017-05-03 12:44:23', null);
INSERT INTO `sys_dictionary` VALUES ('93e4051729ac4586979a52bd5617740f', '97ec40cfa09f4531ad1c8485885fe57b', '4', '定时邮件', null, '1', '1', '4', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('9c63657b98c444e3bfd8a0a75128de2b', '7a7faf68a5ec4f3cb9f45d89c119b26b', '0', '只读', null, '1', '0', '1', '2017-05-03 12:44:23', null, '2017-05-03 12:44:23', null);
INSERT INTO `sys_dictionary` VALUES ('9fc8ed31830c48a7a32f613bce856d3f', 'be4c44e7d83a4f508caa3e893e4c3360', '4', '社保', null, '1', '1', '4', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('a5be19d6d85b424d9e719df8aa4cfdb5', 'be4c44e7d83a4f508caa3e893e4c3360', '1', '教育', null, '1', '1', '1', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('a6b6cce2517943508e1cacde7bb486a8', 'a1a2e8d035934d978e44bbd965db743e', '9', '炭步镇', null, '1', '1', '10', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('a96dfb72b7b54e1989569a2b3c5f90ac', 'c48507ef391d4e3d8d9b7720efe4841b', '1', '启用', null, '1', '0', '1', '2017-05-03 12:44:23', null, '2017-05-03 12:44:23', null);
INSERT INTO `sys_dictionary` VALUES ('ae623964bbb8431d8f62d2adf7784b28', '97ec40cfa09f4531ad1c8485885fe57b', '3', '业务邮件', null, '1', '1', '3', '2017-05-03 12:44:19', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:19', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('b5d9c2328ce749a9913a7cf4def3b743', '6d38696cb3f0457cb69831c3ef3b02b2', '1', '投递成功', null, '1', '1', '1', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('b98e939f0c1a4a658bce8cbcf5c0dbaa', 'a1a2e8d035934d978e44bbd965db743e', '3', '花城街', null, '1', '1', '4', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('c15b3e8b29a0406c81a4318d7bfaba39', 'a1a2e8d035934d978e44bbd965db743e', '1', '新华街', null, '1', '1', '2', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('c6ccaf5afb0d46dcbe1ed50815047291', 'be4c44e7d83a4f508caa3e893e4c3360', '8', '公安局', null, '1', '1', '8', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:22', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('ca40ef37acef49f8930fcf22356ba50e', 'c6f8b99b95c844b89dc86c143e04a294', '1', '是', null, '1', '0', '2', '2017-05-03 12:44:23', null, '2017-05-03 12:44:23', null);
INSERT INTO `sys_dictionary` VALUES ('d2cf230ce49040e3bf6e61a972659c09', '992a7d6dbe7f4009b30cbae97c3b64a9', '2', '停用', 'red', '1', '1', '2', '2017-05-03 12:44:23', null, '2017-05-03 12:44:23', null);
INSERT INTO `sys_dictionary` VALUES ('d404e540aab945df84a26e3d30b2dd47', '820d2a68425b4d8d9b423b81d6a0eec1', '2', '女', null, '1', '1', '2', '2017-05-03 12:44:23', null, '2017-05-03 12:44:23', null);
INSERT INTO `sys_dictionary` VALUES ('d60b600511bd4f1eaeb72c1aab35d229', 'a1a2e8d035934d978e44bbd965db743e', '8', '狮岭镇', null, '1', '1', '9', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('d7f0c4a5480d4dc4b3e6e4c5b405d9cb', '820d2a68425b4d8d9b423b81d6a0eec1', '1', '男', null, '1', '1', '1', '2017-05-03 12:44:23', null, '2017-05-03 12:44:23', null);
INSERT INTO `sys_dictionary` VALUES ('d8642fc74f5e4824b1254705285f1264', '97ec40cfa09f4531ad1c8485885fe57b', '1', '普通邮件', null, '1', '1', '1', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('ddf8fbadb1fc40f49fede49655985db5', 'a1a2e8d035934d978e44bbd965db743e', '10', '梯面镇', null, '1', '1', '11', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('e0e59a52f42c4263aac3e9dbbdb496df', '0bf2a3cd7ed44516a261347d47995411', '1', '经典风格', null, '1', '1', '1', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('e5adcaa5d9cb4612abd14bd935080b50', 'be4c44e7d83a4f508caa3e893e4c3360', '5', '医保', null, '1', '1', '5', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:23', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('e664bd31823a4384ac3f3ce5938094cd', 'a1a2e8d035934d978e44bbd965db743e', '4', '秀全街', null, '1', '1', '5', '2017-05-03 12:44:20', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:20', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('e823709cdff74b1dac9d6a3204af515f', 'a1a2e8d035934d978e44bbd965db743e', '11', '其他区', null, '1', '1', '12', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('ee53ff692f384230b7da89eeef28f2c1', 'a1a2e8d035934d978e44bbd965db743e', '6', '花东镇', null, '1', '1', '7', '2017-05-03 12:44:20', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:20', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary` VALUES ('f1c0ae8844504f96836b904ce81ac1bc', '7a7faf68a5ec4f3cb9f45d89c119b26b', '1', '可编辑', null, '1', '0', '2', '2017-05-03 12:44:23', null, '2017-05-03 12:44:23', null);
INSERT INTO `sys_dictionary` VALUES ('f4309055e8dd450489a85d37b2a4003f', 'a1a2e8d035934d978e44bbd965db743e', '2', '新雅街', null, '1', '1', '3', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:44:21', 'cb33c25f5c664058a111a9b876152317');

-- ----------------------------
-- Table structure for `sys_dictionary_index`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary_index`;
CREATE TABLE `sys_dictionary_index` (
  `dic_index_id` varchar(50) NOT NULL COMMENT '流水号',
  `dic_key` varchar(50) DEFAULT NULL COMMENT '字典标识',
  `dic_name` varchar(100) DEFAULT NULL COMMENT '字典名称',
  `catalog_id` varchar(50) DEFAULT NULL COMMENT '所属分类流水号',
  `catalog_cascade_id` varchar(500) DEFAULT NULL COMMENT '所属分类流节点语义ID',
  `dic_remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`dic_index_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典索引表';

-- ----------------------------
-- Records of sys_dictionary_index
-- ----------------------------
INSERT INTO `sys_dictionary_index` VALUES ('032b31e5b64e40f6b3140e6b96dcbc1c', 'school_type', '学位类型', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary_index` VALUES ('0bf2a3cd7ed44516a261347d47995411', 'layout_style', '界面布局', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary_index` VALUES ('6d38696cb3f0457cb69831c3ef3b02b2', 'email_status', '投递状态', '0e6cca42523b4f95afb8d138dc533e61', '0.002', null, '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary_index` VALUES ('7a7faf68a5ec4f3cb9f45d89c119b26b', 'edit_mode', '编辑模式', 'b420860910f54c1d8901f099a9457f52', '0.002.002', null, '2017-05-03 12:32:40', null, '2017-05-03 12:32:40', null);
INSERT INTO `sys_dictionary_index` VALUES ('820d2a68425b4d8d9b423b81d6a0eec1', 'sex', '性别', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2017-05-03 12:32:40', null, null, null);
INSERT INTO `sys_dictionary_index` VALUES ('97ec40cfa09f4531ad1c8485885fe57b', 'email_type', '邮件类型', '0e6cca42523b4f95afb8d138dc533e61', '0.002', null, '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary_index` VALUES ('992a7d6dbe7f4009b30cbae97c3b64a9', 'user_status', '用户状态', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2017-05-03 12:32:40', null, null, null);
INSERT INTO `sys_dictionary_index` VALUES ('99fd0f3f2d1a49c1acd97ea22415e4a8', 'menu_type', '菜单类型', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary_index` VALUES ('a1a2e8d035934d978e44bbd965db743e', 'street_type', '街镇', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary_index` VALUES ('a9f98697527e452eaa4540e60ae98dc6', 'is_send', '是否发送', '0e6cca42523b4f95afb8d138dc533e61', '0.002', null, '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary_index` VALUES ('aaec0092a25b485f90c20898e9d6765d', 'role_type', '角色类型', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2017-05-03 12:32:40', null, null, null);
INSERT INTO `sys_dictionary_index` VALUES ('be4c44e7d83a4f508caa3e893e4c3360', 'unit_type', '职能单位类型', '6a75051434b840a597aeb549e1e47ced', '0.002.001', null, '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:32:40', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_dictionary_index` VALUES ('c48507ef391d4e3d8d9b7720efe4841b', 'status', '当前状态', 'b420860910f54c1d8901f099a9457f52', '0.002.002', null, '2017-05-03 12:32:40', null, '2017-05-03 12:32:40', null);
INSERT INTO `sys_dictionary_index` VALUES ('c6f8b99b95c844b89dc86c143e04a294', 'is_auto_expand', '是否自动展开', 'b420860910f54c1d8901f099a9457f52', '0.002.002', null, '2017-05-03 12:32:40', null, '2017-05-03 12:32:40', null);

-- ----------------------------
-- Table structure for `sys_icon`
-- ----------------------------
DROP TABLE IF EXISTS `sys_icon`;
CREATE TABLE `sys_icon` (
  `icon_id` varchar(50) NOT NULL COMMENT '图标编号',
  `icon_name` varchar(50) NOT NULL COMMENT '图标名称',
  `icon_file_name` varchar(50) DEFAULT NULL,
  `icon_path` varchar(100) DEFAULT NULL,
  `icon_type` varchar(50) DEFAULT NULL,
  `icon_size_type` varchar(10) DEFAULT NULL COMMENT '图标尺寸类型 1:16*16 2:24*24 3:32*32  4:其它',
  `status` varchar(10) DEFAULT '1' COMMENT '当前状态 0 停用 1启用',
  `edit_mode` varchar(10) DEFAULT '1' COMMENT '编辑模式(0:只读;1:可编辑)',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '图标创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '图标修改人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`icon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图标信息';

-- ----------------------------
-- Records of sys_icon
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` varchar(50) NOT NULL DEFAULT '' COMMENT '菜单编号',
  `cascade_id` varchar(500) DEFAULT NULL COMMENT '分类科目语义ID',
  `menu_name` varchar(100) DEFAULT NULL COMMENT '菜单名称',
  `menu_type` varchar(10) DEFAULT '1' COMMENT '菜单类型 1缺省',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '菜单父级编号',
  `icon_name` varchar(50) DEFAULT NULL COMMENT '图标名称',
  `is_auto_expand` varchar(10) DEFAULT '0' COMMENT '是否自动展开',
  `url` varchar(100) DEFAULT NULL COMMENT 'url地址',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `status` varchar(10) DEFAULT '1' COMMENT '当前状态(0:停用;1:启用)',
  `edit_mode` varchar(10) DEFAULT '1' COMMENT '编辑模式(0:只读;1:可编辑)',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单配置';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('0a6d1c03198a451da47bb3442145bc34', '0.002.003', '学校管理', '1', 'de799fc73ad841d0af51bf7847e3c21d', 'school', '1', 'business/school/init.jhtml', null, '1', '1', '3', '2017-05-03 13:42:35', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 13:42:35', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_menu` VALUES ('1ae9eeb251a243abb4c0c4f3865d6262', '0.001.001', '菜单配置', '1', 'c66886c9ee47415aa81a6589acdb480a', 'menu_config', '1', 'system/menu/init.jhtml', null, '1', '1', '4', '2016-10-07 21:23:38', null, null, null);
INSERT INTO `sys_menu` VALUES ('60dea2ab5364424f85e9d920569f7fc9', '0.001.010', '微信菜单', '1', 'c66886c9ee47415aa81a6589acdb480a', null, '1', 'system/wechatMenu/init.jhtml', null, '1', '1', '4', '2017-07-07 22:01:18', 'cb33c25f5c664058a111a9b876152317', '2017-07-07 22:01:18', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_menu` VALUES ('7f4f6a3b2a0d48f998414d01a00fcbb3', '0.002.001', '邮件管理', '1', 'de799fc73ad841d0af51bf7847e3c21d', 'email', '1', 'system/email/initEmailRecord.jhtml', null, '1', '1', '1', '2017-05-03 13:41:48', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 13:41:48', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_menu` VALUES ('8eefe6e3298c4203b21e85e354b284ab', '0.001.004', '分类科目', '1', 'c66886c9ee47415aa81a6589acdb480a', 'catalog', '1', 'system/catalog/init.jhtml', null, '1', '1', '7', '2016-10-09 21:23:47', null, null, null);
INSERT INTO `sys_menu` VALUES ('94ca69dd59b84054ad056b130d959425', '0.001.003', '键值参数', '1', 'c66886c9ee47415aa81a6589acdb480a', 'param_config', '1', 'system/param/init.jhtml', null, '1', '1', '6', '2016-10-09 21:22:03', null, null, null);
INSERT INTO `sys_menu` VALUES ('a5b39c90931b4249a23e30f24303dbfa', '0.001.002', '数据字典', '1', 'c66886c9ee47415aa81a6589acdb480a', 'dictionary', '1', 'system/dictionary/init.jhtml', '', '1', '0', '5', '2016-10-07 21:40:52', null, '2016-10-07 22:12:51', null);
INSERT INTO `sys_menu` VALUES ('c66886c9ee47415aa81a6589acdb480a', '0.001', '系统管理', '1', '0', 'system_manage', '1', '', '', '1', '1', '10', '2016-10-07 17:57:14', null, '2017-02-09 11:54:32', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_menu` VALUES ('d525b1f3274244c9af3a06c4e72621d8', '0.001.009', '角色管理', '1', 'c66886c9ee47415aa81a6589acdb480a', 'group_link', '1', 'system/role/init.jhtml', '', '1', '1', '3', '2016-12-31 09:09:32', null, '2017-01-15 19:32:18', null);
INSERT INTO `sys_menu` VALUES ('d806eed4d2ce411f807422dbd1df35e5', '0.002.002', '单位管理', '1', 'de799fc73ad841d0af51bf7847e3c21d', 'unit_config', '1', 'business/unit/init.jhtml', null, '1', '1', '2', '2017-05-03 13:42:35', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 13:42:35', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_menu` VALUES ('de799fc73ad841d0af51bf7847e3c21d', '0.002', '信息管理', '1', '0', 'msg_config', '1', null, null, '1', '1', '2', '2017-05-03 13:41:48', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 13:41:48', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_menu` VALUES ('dfc4df3c7f4341f885caf7aa305f8995', '0.001.007', ' 组织机构', '1', 'c66886c9ee47415aa81a6589acdb480a', 'dept_config', '1', 'system/dept/init.jhtml', '', '1', '1', '1', '2016-12-11 08:38:33', null, '2016-12-11 21:59:55', null);
INSERT INTO `sys_menu` VALUES ('f278c724ce8e4649bc2b74333ac0d28c', '0.001.008', '用户管理', '1', 'c66886c9ee47415aa81a6589acdb480a', 'user_config', '1', 'system/user/init.jhtml', '', '1', '1', '2', '2016-12-18 08:41:02', null, '2017-01-15 20:03:32', null);

-- ----------------------------
-- Table structure for `sys_param`
-- ----------------------------
DROP TABLE IF EXISTS `sys_param`;
CREATE TABLE `sys_param` (
  `param_id` varchar(50) NOT NULL DEFAULT '' COMMENT '参数编号',
  `param_name` varchar(100) DEFAULT NULL COMMENT '参数名称',
  `param_key` varchar(50) DEFAULT NULL COMMENT '参数键名',
  `param_value` varchar(500) DEFAULT NULL COMMENT '参数键值',
  `catalog_id` varchar(50) DEFAULT NULL COMMENT '目录ID',
  `catalog_cascade_id` varchar(500) DEFAULT NULL COMMENT '分类科目语义ID',
  `param_remark` varchar(200) DEFAULT NULL COMMENT '参数备注',
  `status` varchar(10) DEFAULT '1' COMMENT '当前状态(0:停用;1:启用)',
  `edit_mode` varchar(10) DEFAULT '1' COMMENT '编辑模式(0:只读;1:可编辑)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户ID',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`param_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='键值参数';

-- ----------------------------
-- Records of sys_param
-- ----------------------------
INSERT INTO `sys_param` VALUES ('7615a2c43b2b4b28a63703a4b005cb60', '欢迎消息', 'welcome_msg', 'IMS欢迎关注微信号', '4f39839093744dccaedc9d7dcdee4ab3', '0.001', null, '1', '1', '2017-07-07 21:58:31', 'cb33c25f5c664058a111a9b876152317', '2017-07-07 21:58:31', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_param` VALUES ('c4df22d4d63d4d5ea136eff79b65dc69', '邮件开关', 'email_switch', 'on', '4f39839093744dccaedc9d7dcdee4ab3', '0.001', '邮件开关 on:开 off:关', '1', '1', '2017-05-03 12:33:34', 'cb33c25f5c664058a111a9b876152317', '2017-05-03 12:33:34', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_param` VALUES ('dc6ef86008804971bec34b1b1b2df889', '系统请求地址', 'request_url', 'http://ims.ngrok.xiaomiqiu.cn/IMS', '4f39839093744dccaedc9d7dcdee4ab3', '0.001', null, '1', '1', '2017-07-07 21:57:27', 'cb33c25f5c664058a111a9b876152317', '2017-07-07 21:57:27', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `sys_param` VALUES ('f73843b87bfd44caa0e09faf1d8673e0', '系统默认回复信息', 'response_msg', '谢谢你关注IMS信息', '4f39839093744dccaedc9d7dcdee4ab3', '0.001', null, '1', '1', '2017-07-07 21:59:24', 'cb33c25f5c664058a111a9b876152317', '2017-07-07 21:59:24', 'cb33c25f5c664058a111a9b876152317');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` varchar(50) NOT NULL COMMENT ' 流水号',
  `role_name` varchar(100) NOT NULL COMMENT '角色名称',
  `status` varchar(10) DEFAULT '1' COMMENT '当前状态 1启用 0禁用',
  `role_type` varchar(10) DEFAULT NULL COMMENT '角色类型',
  `role_remark` varchar(400) DEFAULT NULL COMMENT '备注',
  `edit_mode` varchar(10) DEFAULT '1' COMMENT '编辑模式(0:只读;1:可编辑)',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` varchar(50) NOT NULL COMMENT ' 角色流水号',
  `menu_id` varchar(50) NOT NULL COMMENT '功能模块流水号',
  `grant_type` varchar(10) DEFAULT NULL COMMENT '权限类型 1 经办权限 2管理权限',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单模块-角色关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `role_id` varchar(50) NOT NULL COMMENT '角色编号',
  `user_id` varchar(50) NOT NULL COMMENT '用户编号',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与用户关联';

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` varchar(50) NOT NULL COMMENT '用户编号',
  `account` varchar(50) NOT NULL COMMENT '用户登录帐号',
  `password` varchar(50) NOT NULL COMMENT '密码',
  `username` varchar(50) NOT NULL COMMENT '用户姓名',
  `lock_num` int(10) DEFAULT '5' COMMENT '锁定次数 默认5次',
  `error_num` int(10) DEFAULT '0' COMMENT '密码错误次数  如果等于锁定次数就自动锁定用户',
  `sex` varchar(10) DEFAULT '3' COMMENT '性别  1:男2:女3:未知',
  `status` varchar(10) DEFAULT '1' COMMENT '用户状态 1:正常2:停用 3:锁定',
  `user_type` varchar(10) DEFAULT NULL COMMENT '用户类型',
  `dept_id` varchar(50) DEFAULT NULL COMMENT '所属部门流水号',
  `mobile` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `qq` varchar(50) DEFAULT NULL COMMENT 'QQ号码',
  `wechat` varchar(50) DEFAULT NULL COMMENT '微信',
  `email` varchar(50) DEFAULT NULL COMMENT '电子邮件',
  `idno` varchar(50) DEFAULT NULL COMMENT '身份证号',
  `style` varchar(10) DEFAULT '1' COMMENT '界面风格',
  `address` varchar(200) DEFAULT NULL COMMENT '联系地址',
  `remark` varchar(400) DEFAULT NULL COMMENT '备注',
  `is_del` varchar(10) DEFAULT '0' COMMENT '是否已删除 0有效 1删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建人ID',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户编号',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户基本信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('cb33c25f5c664058a111a9b876152317', 'super', '0d+ywCe6ffI=', '超级用户', '10', '0', '1', '1', '2', '0', '13802907704', '240823329', '', '240823329@qq.com', '', '1', '', '超级用户，拥有最高的权限', '0', '2017-01-15 09:47:46', null, '2017-07-07 22:06:07', 'cb33c25f5c664058a111a9b876152317');

-- ----------------------------
-- Table structure for `wechat_menu`
-- ----------------------------
DROP TABLE IF EXISTS `wechat_menu`;
CREATE TABLE `wechat_menu` (
  `menu_id` varchar(50) NOT NULL COMMENT '菜单编号',
  `menu_name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父级菜单编号',
  `menu_type` varchar(50) DEFAULT NULL COMMENT '菜单类型',
  `menu_key` varchar(50) DEFAULT NULL COMMENT '菜单键值',
  `url` varchar(500) DEFAULT NULL COMMENT '菜单URL',
  `media_id` varchar(50) DEFAULT NULL COMMENT 'media_id',
  `template_id` varchar(50) DEFAULT NULL COMMENT '消息模板编号',
  `is_auto_expand` varchar(10) DEFAULT '0' COMMENT '是否自动展开',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户编号',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` varchar(50) DEFAULT NULL COMMENT '修改用户ID',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信菜单信息';

-- ----------------------------
-- Records of wechat_menu
-- ----------------------------
INSERT INTO `wechat_menu` VALUES ('0a4f55e2d5204f318290a671f521f56f', '更多', 'c8f4944b81054076912219abf76a9ac3', 'view', 'V20005', 'https://h5.youzan.com/v2/showcase/homepage?alias=rdh4pva9', '', null, '1', '5', '', '2017-06-17 07:55:44', 'cb33c25f5c664058a111a9b876152317', '2017-06-19 21:12:16', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `wechat_menu` VALUES ('0b5956906c124f559ac82dc2957c40a3', '美白战警', 'c8f4944b81054076912219abf76a9ac3', 'view', 'V20002', 'https://shop19212393.youzan.com/v2/goods/1y6hd2013t35d?reft=1497847785986&spm=f47651224&sf=wx_sm', '', null, '1', '3', '', '2017-06-17 08:02:12', 'cb33c25f5c664058a111a9b876152317', '2017-06-19 23:17:02', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `wechat_menu` VALUES ('39d3d847f7ce4526bca7c619af81db16', '缉油特工', 'c8f4944b81054076912219abf76a9ac3', 'view', 'V20001', 'https://shop19212393.youzan.com/v2/goods/3nu6wr8tjhrfl?reft=1497847762556&spm=f47651224&sf=wx_sm&redirect_count=1', '', null, '1', '2', '', '2017-06-17 07:57:48', 'cb33c25f5c664058a111a9b876152317', '2017-06-19 23:16:50', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `wechat_menu` VALUES ('84891f85338a41fab8c46a2c2fa1cbbe', '我的', '0', 'view', 'V10003', 'wechat/home/goHome.jhtml', '', null, '1', '3', '', '2017-05-04 00:41:15', 'cb33c25f5c664058a111a9b876152317', '2017-05-20 08:39:58', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `wechat_menu` VALUES ('c8f4944b81054076912219abf76a9ac3', '找项目', '0', 'click', 'V10001', '', '', null, '1', '1', '', '2017-05-03 22:47:13', 'cb33c25f5c664058a111a9b876152317', '2017-06-17 07:52:04', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `wechat_menu` VALUES ('e0f24602472a47089b253f905548edcc', '找店铺', '0', 'view', 'V10002', 'wechat/shop/goFindShop.jhtml', '', null, '1', '2', '', '2017-05-04 00:39:51', 'cb33c25f5c664058a111a9b876152317', '2017-06-19 23:20:07', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `wechat_menu` VALUES ('e3361b68c20641588eca5b38947c835d', '大礼包', 'c8f4944b81054076912219abf76a9ac3', 'view', 'V20006', 'wechat/bag/goBag.jhtml', '', null, '1', '1', '', '2017-06-19 23:16:39', 'cb33c25f5c664058a111a9b876152317', '2017-06-20 01:13:15', 'cb33c25f5c664058a111a9b876152317');
INSERT INTO `wechat_menu` VALUES ('ec50c7b3aeb14bc0a21f9c4b66dfa796', '上镜神器', 'c8f4944b81054076912219abf76a9ac3', 'view', 'V20003', 'https://h5.youzan.com/v2/showcase/emptypage?kdt_id=19020225', '', null, '1', '4', '', '2017-06-17 08:03:17', 'cb33c25f5c664058a111a9b876152317', '2017-06-19 23:17:10', 'cb33c25f5c664058a111a9b876152317');
