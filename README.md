###  IMS综合开发平台
本开发平台是基于开源平台AOS基础建立起来，并在此基础进行更新完善，并加入本人工作中经常使用的工具类库，并采用不同的UI框架进行前端布局，目前只提供
Easyui ，后续版本将提供dwz版本，bootstrap版本。本系统已支持读写分离的数据库操作，并加入阿里里巴巴的连接池driud，只需要替换配置文件即可。支持开源主流的数据库mysql，oracle，sqlserver，并提供这三种数据库的sql脚本。系统支持redis和Ehcache 两种缓存，有系统自动检测，如果redis在线，优先使用redis缓存。界面支持两种布局经典布局和顶部菜单布局，并封装了常用的easyui函数，本系统是采用高版本的spring4.3进行开发!
  **项目演示地址** ：http://118.89.36.182:8086
 **系统超级用户:super 密码:super** 
 **微云下载地址:** https://share.weiyun.com/8c3b880cfa450bddd1ae62d349184f20
 **百度云盘下载:** http://pan.baidu.com/s/1c19F9Zy
 **QQ交流群：223660747** 
[![输入图片说明](http://git.oschina.net/uploads/images/2017/0209/161209_86a56f32_436510.png "在这里输入图片标题")](http://git.oschina.net/uploads/images/2017/0209/161151_ad39b5d3_436510.png "在这里输入图片标题")
你的鼓励将是我最大的支持
![输入图片说明](https://git.oschina.net/uploads/images/2017/0814/111124_0b3a1336_436510.png "屏幕截图.png")![输入图片说明](https://git.oschina.net/uploads/images/2017/0814/111137_4628637b_436510.png "屏幕截图.png")

 